<div style="text-align:right"><b>Ly Rainui 1A SN App</b></div>
<div style="text-align:center"><h1>Système de gestion de fichiers</h1></div>
[TOC]

<div style="page-break-after: always; break-after: page;"></div>
## Préambule

Le projet de *méthodologie de programmation* représente selon moi un moyen de mettre à profit les compétences apprises pendant le module. Ce rapport retrace par écrit le procédé mis en place pour la réalisation de ce projet. Nous y retrouvons l'architecture choisie pour le mener à bien ainsi que le dénouement du projet accompagné d'un bilan technique et personnel.

## Introduction

Le projet consiste à réaliser un système de gestion de fichiers minimale qui reprend les fonctionnalités de bases, à savoir la manipulation et la localisation des fichiers et l'allocation de la place sur mémoire. Ce système, utilisé dans tous systèmes d'exploitations, opère de manière transparente pour l'utilisateur. L'interface qu'il implémente prend en compte plusieurs notions telles que : les métadonnées, l'arborescence et les parcours utilisant la notion de chemin absolu et relatif. L'objectif du projet est de produire un tel système simulé par un programme écrit en Ada. Ce programme doit regrouper tous les concepts abordés durant le module *méthodologie de la programmation*.

Ce rapport est réparti en plusieurs sections. La première concerne l'architecture choisie pour implanter le système de gestion de fichiers. Ensuite, on retrouve les choix techniques, puis les algorithmes et types de données implémentés pour modéliser le système. La section de tests correspond aux moyens mises en œuvres pour assurer le fonctionnement du programme et de ses dépendances. Enfin, il se termine par les difficultés rencontrées et les solutions appliquées durant la réalisation de ce projet avant de conclure par un bilan personnel et technique à l'issue des 18 séances de projet prévues.

<div style="page-break-after: always; break-after: page;"></div>
## Architecture du système de gestion de fichiers

### Vue d'ensemble

Pour concevoir un système capable de gérer les notions d'arborescence, de parcours et de métadonnées, le choix d'une architecture incluant une structure d'arbre est indispensable. Cela permet d'organiser les données en mémoire de manière logique et hiérarchisée. À cela, une interface est nécessaire pour rendre possible l'interaction de l'utilisateur sur la structure contenant l'ensemble des données. Cette structure composée de nœuds, représentant les métadonnées, est *contrôlée* par un dernier composant chargé de lier les interactions de l'utilisateur à cette dernière.



![Architecture du SGF](rsc/architecture.png)



Ainsi, nous avons une architecture comprenant :

- **Structure d'arbre** : organisation des données à manipuler.
- **Contrôleur** : *parser* des commandes utilisateurs et des résultats représentés par la structure de l'arbre.
- **Interface utilisateur** : interpréteur des commandes lancées par l'utilisateur.

### Gestion de la mémoire

Concernant la gestion de mémoire, une des solutions envisagées à été de concevoir un tableau de dimension **n** telle que la dimension représente le nombre de cases nécessaire pour valoir la taille du disque. Chacune de ses cases équivaut à 10 Ko, soit la taille par défaut d'une métadonnée selon la deuxième partie du projet. Pour 1 To de mémoire disque, nous aurons un tableau de 1 000 000 000 de cases. Toutefois, il est à notifier que la contrainte matérielle et physique qu'impose cette solution est non négligeable. Ainsi, il est plus judicieux de créer une liste chaînée contenant au plus 1024 cellules, où chaque cellule contient un tableau de 1 000 000 de cases, ce qui est plus raisonnable.

De cette manière, chaque métadonnée possédera une adresse unique, représentée par un entier, qui s'utilise comme indice de tableau pour avoir un accès direct a ce dernier. Voici donc les cas de figures possibles à cette structure de mémoire :

- **Création d'une nouvelle donnée :** remplissage ordonné des cases du 1er tableau jusqu'au ième tableau contenu dans la ième cellule de la liste chaînée.
- **Suppression d'une donnée :** mise à `null` de la jème case du tableau par l'adresse de cette donnée (accès direct).
  - Si la donnée se trouve dans le ième tableau (adresse strictement supérieur à 1 000 000), il suffit de moduler l'adresse de la donnée par 1 000 000 à chaque changement de cellule de la liste chaînée, car chaque tableau est indicé de 1 à 1 000 000. 
- **Libération de la mémoire :** lorsqu'une donnée est supprimée, elle libère de la mémoire en laissant la case du tableau vide. Pour éviter de laisser ses cases vides inutilisées, les valeurs sont décalés *vers la gauche* à chaque fois que la dernière case du tableau est rempli afin de rester sur une gestion de mémoire contiguë. L'adresse des métadonnées se mettra à jour en conséquence.

## Les choix réalisés

### Choix métiers

Dans un premier temps, il a été question de fixer des *priorités métiers*, c'est-à-dire de décider des capacités du système à réaliser afin de produire un <u>système fonctionnel</u> sur le temps imparti en envisageant la possibilité que des fonctionnalités peuvent être manquantes. 

Voici ce qui a été priorisé dans un premier temps :

| Indispensable                               | Si possible                                            |
| ------------------------------------------- | ------------------------------------------------------ |
| Interface utilisateur sous forme de menu    | Interface utilisateur en ligne de commandes            |
| La création de données élémentaires         | La création de données archivées                       |
| La suppression des données élémentaires     | L'affichage détaillé et l'affichage sous forme d'arbre |
| Au moins un affichage des données           | Le déplacement des données élémentaires                |
| Notion de chemin relatif et absolu          | La copie des données élémentaires                      |
| Le déplacement dans la structure de données | Prise en charge de la mémoire                          |

### Choix fonctionnels

Dans un second temps, l'enjeu a été de modéliser en détails la structure d'arbre. Effectivement, nous avons opté pour les **arbres n-aires** où un nœud de l'arbre représente la métadonnée *dossier* et une feuille de l'arbre représente la métadonnée *fichier*. De cette manière, il y a d'une part, la relation <u>parent-enfant</u> qui modélise un dossier contenant des sous-dossiers ou des fichiers, et d'autre part, la notion de racine unique qu'implémente tout système de gestion de fichiers.

![arbre n-aires sur developpez.com](https://algo.developpez.com/images/faq/StructuresArborescentes/arbre_n_aire.png)



## Les types de données et algorithmes

La mise en œuvre des spécifications énumérées plus haut a été conçue *from scratch*.

### Les types de données

#### Arbre n-aire

Pour modéliser un arbre n-aires, nous avons utilisé les capacités **des pointeurs** pour créer des relations de dépendance entre chaque métadonnée. On retrouve, pour un nœud donné, son parent, ses enfants et sa valeur.

```ada
type t_node; 

type ptr_node is access t_node; -- pointeur de noeud

-- caracterisation du noeud
type t_node is record
	parent : ptr_node; -- pointeur sur noeud parent
	child : ptr_cell; -- pointeur sur ma liste d'enfant 
	value : T; -- mes meta-donnees
end record;	
```

La possibilité qu'un nœud puisse posséder plusieurs enfants nous a orienté vers l'implantation d'une seconde structure de données algorithmique : **la liste chaînée**. Ce qui se caractérise ainsi :

![Arbre n-aire d'après notre spécification](rsc/arbre-naire-schema.png)



#### Liste chaînée

Dans mon programme Ada, la liste chaînée est implémentée comme ceci :

```ada
type t_cell;

type ptr_cell is access t_cell; -- pointeur de cellule

-- caracterisation de la cellule
type t_cell is record
    current : T; -- pointeur sur la cellule courante
    next : ptr_cell; -- pointeur sur la cellule suivante
end record;
```

#### Métadonnées

Pour représenter les données que notre système doit gérer, nous avons produit **un enregistrement** qui contient les champs suivantes :

```ada
-- caracterisation des metadonnees
type t_metadata is record
    name : unbounded_string; -- nom de la metadonnee
    directory : boolean; -- type dossier ou fichier
    rights : t_access_rights; -- droits d'acces a la metadonnee
    creation_time : t_date; -- date de creation de la metadonnee 
    size : integer := 0; -- taille de la metadonnee
end record;
```

Ainsi, une métadonnée est caractérisée par un nom, son type (dossier ou fichier), ses droits d'accès, sa date de création et sa taille en octet.

##### Les droits d'accès

Les droits d'accès ont été modélisés par un enregistrement contenant trois entiers pour les gérer avec des valeurs octales.

```ada
-- caracterisation des droits d'acces par valeur octale (de 0 a 7)
type t_access_rights is record 
    owner : integer; -- droit du groupe owner
    group : integer; -- droit du groupe group
    other : integer; -- droit du groupe other
end record;
```

##### La date de création

La date de création s'appuie sur un module prédéfini par le langage Ada `ada.calendar.*` et reprend uniquement les champs qui nous intéressent à savoir :

```ada
-- caracterisation de la data de creation
type t_date is record
    year : year_number;
    month : month_number;
    day : day_number;
    second : day_duration;
end record;
```

### Les algorithmes

Étant donné les structures de données définies, les algorithmes utilisés ont été vus en cours (*la liste chaînée*) et en travaux pratiques (*l'arbre*) et ont été adaptés au besoin du projet. Ainsi, nous retrouvons principalement des algorithmes de parcours et des algorithmes de mise à jour des données dans une structure.

#### Algorithmes de parcours

Dans notre arbre n-aire, toute méthode nécessitant le parcours de l'arbre possède le squelette suivant.

```
ParcourirArbre(Noeud noeudCourant):
    SI NonVide(noeudCourant) ALORS
        TANT QUE NonVide(noeudCourant.enfants) FAIRE
            ParcourirArbre(noeudCourant);
            noeudCourant <- noeudCourant.suivant;
        FIN TANT QUE
    FIN SI;
```

De même pour les listes chaînées :

```
ParcourirListe(Cellule celluleCourante):
	TANT QUE NonVide(celluleCourante) FAIRE
		celluleCourante <- celluleCourante.suivant;
	FIN TANT QUE
```

#### Algorithmes de mise à jour

Dans notre arbre n-aire, nous sommes amenés à ajouter, supprimer ou déplacer des métadonnées. Pour se faire, les méthodes suivent **la structure suivante**, en admettant qu'un algorithme de parcours positionne notre nœud courant à l'endroit souhaité :

```
UpdateArbre(Noeud noeudCourant, Noeud nouveauNoeud):
	UpdateListe(noeudCourant.enfants, nouveauNoeud);
```

avec la structure de mise à jour pour les listes chaînées :

```
UpdateListe(Cellule entete, Cellule nouvelleCellule):
	Update(entete,nouvelleCellule);
```

et la méthode `Update(entete,nouvelleCellule)`une supposée méthode qui met à jour la structure de données en fonction du besoin (*ajout, suppression, ...*).

## Tester c'est s'assurer

Entreprendre un projet complexe comme celui-ci requiert une bonne organisation et un moyen fiable de garantir le bon fonctionnement des résultats attendus. Pour cela, nous avons mis en place des fichiers de test ainsi qu'un paquetage `adatest` qui permet d'homogénéiser l'apparence des codes de test.

La politique de tests mise en place tend à ressembler aux **tests unitaires** sans pour autant profiter de méthode de type `assert` (comme par exemple `assertEquals()` en Java). Ainsi, pour chaque module, nous avons produit un fichier de test associé se nommant de la façon suivante : `test_<nom_du_module_testé>.adb`.

Ce fichier en question est formé tel que :

```ada
with adatest, monModule;
use adatest, monModule;

procedure test_monModule is

	-- procedure de test de la methode concernee
	procedure test_monModuleMethode1 is
	
	begin
		testname("TEST METHODE 1"); -- affiche le nom de la methode teste
		testcase("cas 1"); -- affiche le cas a tester dans la console
		if methode1() /= resultat_attendu then
			raise adatest.TEST_FAILED; 
		end if;
		testresult(1); -- reussite du test
		testcase("cas 2 (exception expected)");
		begin
			methode1(100);
			raise adatest.TEST_FAILED;
		exception
			when monModule.MON_MODULE_ERROR => testresult(1); -- reussite du test
		end;
	exception
		when adatest.TEST_FAILED => testresult(0); -- echec du test
	end test_monModuleMethode1;

begin
	test_monModuleMethode1;
end test_monModule;
```

Inclure des tests pour chacun des modules créé nous permet de garantir le bon fonctionnement attendu de chaque fonction et procédure du module à tout instant (lors de modification, de *refactoring*...).

<div style="page-break-after: always; break-after: page;"></div>
## Difficultés rencontrées et solutions

La principale difficulté a été d'établir une vision concrète et correcte du projet avant son développement afin de ne pas perdre de temps sur l'implémentation du projet. Cependant, il s'est avéré que les premières ébauche de structure de données ont complexifié le projet tout en menant ce dernier vers une impasse. Ensuite, la phase de spécifications des méthodes du projet, sans possibilité de débogage, a constitué un frein à son avancement.

La solution a été de décomposer, étape par étape et composant par composant, le projet tout en privilégiant les tests unitaires aux codages des méthodes. Le fait de commencer par les tests plutôt que le corps des méthodes a produit une vision claire sur les objectifs et résultats souhaités ainsi que les cas contraignants à éviter ou à traiter.

La logique de développement peut s'illustrer comme ceci :



![Schematisation de la solution de développement](rsc/difficultes.png)

<div style="page-break-after: always; break-after: page;"></div>
## Bilan

### Technique

Par rapport aux [choix métiers](#Choix-métiers) fixés au début du projet, on remarque en premier lieu que les objectifs qualifiés d'indispensables ont été tenus. Ensuite, le bon avancement du projet a permis la réalisation de plusieurs objectifs secondaires dont l'interface en ligne de commandes et l'implémentation de toutes les commandes imposées par le sujet. 

Ainsi, le bilan technique de ce projet est globalement positif malgré l'absence de la seconde partie et de la prise en charge de la mémoire qui n'ont point été abordées.

|       Les accomplissements primaires        |            Les accomplissements secondaires            |         Les inachevés         |
| :-----------------------------------------: | :----------------------------------------------------: | :---------------------------: |
|  Interface utilisateur sous forme de menu   |      Interface utilisateur en ligne de commandes       | Prise en charge de la mémoire |
|     La création de données élémentaires     |                 La création de données                 |                               |
|   La suppression des données élémentaires   | L'affichage détaillé et l'affichage sous forme d'arbre |                               |
|      Au moins un affichage des données      |        Le déplacement des données élémentaires         |                               |
|     Notion de chemin relatif et absolu      |           La copie des données élémentaires            |                               |
| Le déplacement dans la structure de données |                                                        |                               |

### Personnel

En ce qui concerne le bilan personnel, ce projet a été la concrétisation de toutes les séances du module *méthodologie de la programmation* et la vérification des acquis sur les compétences et notions abordés en travaux pratiques.

Il est important de souligner que ce projet **a été travaillé seul** et **en dehors** des heures prévues pour deux principales raisons. La première est due à la qualité du sujet. Effectivement, la conception d'un système de gestion de fichiers est très intéressante sous plusieurs axes. Tout d'abord, parce qu'en tant qu'informaticien, nous utilisons quotidiennement ce type de système et ensuite, parce que ce projet nous a poussé à tendre notre réflexion vers celle qu'avaient les concepteurs des premiers systèmes de gestion de fichiers. La dernière raison a été l'évidence que le projet serait infaisable en 18 séances.

En conclusion, le projet de *méthodologie de programmation* m'a permis de mettre en application mes compétences acquises lors du premier semestre à l'ENSEEIHT et de comprendre les systèmes de gestion de fichiers.