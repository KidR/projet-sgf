with ada.text_io, ada.integer_text_io, ada.strings.unbounded;
use ada.text_io, ada.integer_text_io, ada.strings.unbounded;

with metadata, list;
use metadata;

generic
    type T is private; -- arbre d'element T generique 
    with function getName(X : in T) return String; -- methode d'affichage de l'arbre

package tree is

    NODE_ERROR : exception; -- exception possible sur l'arbre n-aire

    STR_NODE_NULL : string := "null-type is not allowed.";

    type t_node; -- type du noeud

    type ptr_node is access t_node; -- pointeur de noeud

    -- instanciation d'une liste chainee de type ptr_node
    package list_node is new list(ptr_node); 
    use list_node;

    -- caracterisation du noeud
    type t_node is record
        parent : ptr_node; -- pointeur sur noeud parent
        child : ptr_cell; -- pointeur sur ma liste d'enfant 
        value : T; -- mes meta-donnees
    end record;	

    -- Initialise l'arbre
    -- parametres : root le noeud
    -- exception : NODE_ERROR si root est null
    -- post-condition : initiliase root comme racine de l'arbre
    procedure initTree(root : out ptr_node);

    -- Retourne la racine de l'arbre
    -- parametres : src le noeud source
    -- exception : NODE_ERROR si root est null
    -- post-condition : retourne le noeud racine
    function getRoot(src : in ptr_node) return ptr_node;

    -- Initialise la racine de l'arbre
    -- parametres : root le noeud racine
    -- post-condition : root devient un noeud racine
    procedure setRoot(root: in out ptr_node);

    -- Verifie si le noeud est la racine de l'arbre
    -- parametres : src le noeud source
    -- retour : boolean
    -- exception : NODE_ERROR si root est null
    -- post-condition : vrai si src racine faux sinon
    function isRoot(src : in ptr_node) return boolean;

    -- Verifie si le noeud courant possede des enfants
    -- parametres : src le noeud source
    -- retour : boolean
    -- exception : NODE_ERROR si root est null
    -- post-condition : vrai si possede au moins un enfant faux sinon
    function hasChild(src : in ptr_node) return boolean;

    -- Verifie si le noeud courant possede l'enfant node
    -- parametres : src le noeud source, node le noeud enfant
    -- retour : boolean
    -- exception : NODE_ERROR si root est null
    -- post-condition : vrai si src parent de node faux sinon
    function hasChild(src : in ptr_node; node : in ptr_node) return boolean;

    -- Verifie si le noeud courant possede l'enfant de nom name
    -- parametres : src le noeud source, name le nom du noeud
    -- retour : boolean
    -- exception : NODE_ERROR si root est null
    -- post-condition : vrai si src parent du noeud dont le nom est name faux sinon
    function hasChild(src : in ptr_node; name : in string) return boolean;

    -- Verifie si le noeud courant possede le noeud parent
    -- parametres : src le noeud source, parent le noeud parent
    -- retour : boolean
    -- exception : NODE_ERROR si src est le noeud racine
    -- post-condition : vrai si parent parent de src faux sinon
    function hasParent(src : in ptr_node; parent : in ptr_node) return boolean;

    -- Ajoute un fils au noeud courant
    -- parametres : src le noeud source, child le noeud enfant
    -- exception : NODE_ERROR si src ou child sont null ou si child deja enfant de src
    -- post-condition : child est le noeud enfant de src
    procedure addChild(src : in out ptr_node; child : in ptr_node);

    -- Supprime un fils au noeud courant
    -- parametres : src le noeud source, child le noeud enfant
    -- exception : NODE_ERROR si src ou child sont null
    -- post-condition : src n'est plus parent de child
    procedure removeChild(src : in out ptr_node; child : in ptr_node);

    -- Affiche une representation arborescente de l'arbre
    -- parametres : src le noeud source, level nombre tabulation
    -- exception : NODE_ERROR si un noeud de l'arbre est null
    -- post-condition : affiche sous forme d'arbre tous les noeuds a partir de src
    procedure printTree(src : in ptr_node; level : in integer := 0);

    -- Retourne le chemin d'un noeud a partir du noeud courant
    -- parametres : src le noeud source, node le noeud recherche
    -- retour : string
    -- exception : NODE_ERROR si src est null
    -- post-condition : renvoie sous forme de chemin le noeud node depuis src
    function getPath(src : in ptr_node; node : in ptr_node) return string; 
    
    -- Retourne le noeud a partir de sa valeur
    -- parametres : src le noeud source, name le nom de l'enfant
    -- retour : ptr_node
    -- exception : NODE_ERROR si src ou ses enfants sont null 
    -- post-condition : le noeud renvoie a pour ancestre le noeud src et porte le nom name
    function getChild(src : in ptr_node; name : in string) return ptr_node;

end;
