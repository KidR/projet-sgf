with ada.text_io, ada.integer_text_io, ada.strings.unbounded;
use ada.text_io, ada.integer_text_io, ada.strings.unbounded;

with metadata;
use metadata;

with tree, list;

package rshell is

    -- instanciation d'un arbre de metadonnees
    package tree_metadata is new tree(t_metadata,getName);
    use tree_metadata;

    -- instanciation d'une liste de chaine de caractere pour former un chemin
    package list_string is new list(unbounded_string);
    use list_string;

    SYMBOLE_CURRENT_DIR : string := ".";

    SYMBOLE_PARENT_DIR : string := "..";

    SYMBOLE_ROOT_DIR : string := "/";

    PATH_ERROR : exception; -- exception lors d'une erreur de chemin

    -- Enumeration des commandes
    type Cmd is (ls, ll, tree, cd, pwd, touch, mkdir, tar, rm, rr, mv, cp);

    -- pointeur de procedure create
    type pr_ptr_create is access procedure(obj : out t_metadata; name : in string; rights : in string);
    
    -- pointeur de procedure find
    type pr_ptr_find is access procedure(src: in out ptr_node; dst : in ptr_node);
    
    -- pointeur de fonction print
    type f_ptr_print is access function(src : in t_metadata) return string;

    -- Scinde une chaine de caractere par toutes les occurences de la sous chaine c
    -- parametres : res liste chainee de chaine, str la chaine a decouper, c la sous-chaine a decouper
    -- post-condition : construit une liste chainee constituant la chaine passe en parametre 
    procedure splitString(res : out ptr_cell; str : in string; c : in string);

    -- Scinde une chaine de caractere par toutes les occurences de la sous chaine "/"
    -- parametres : res liste chainee de chaine, str le chemin a decouper
    -- post-condition : construit une liste de chaine constituant le chemin passe en parametre
    procedure splitPath(res : out ptr_cell; str : in string);

    -- Navigue a partir d'un noeud source vers une destination avec la notion de chemin relative et absolu
    -- parametres : src le noeud source, path le chemin a parcourir
    -- post-condition : src est positionne sur le chemin path
    procedure processCd(src : in out ptr_node; path : in string);

    -- Affiche les metadonnees presentes dans le noeud
    -- parametres : src le noeud source
    -- post-conditions : affiche les enfants (metadonees) du noeud src
    procedure cmdLs(src : in ptr_node);

    -- Affiche les metadonnees presentes dans le noeud en detail
    -- parametres : src le noeud source
    -- post-conditions : affiche les enfants (metadonees) du noeud src avec toutes leurs informations
    procedure cmdLsLong(src : in ptr_node);

    -- Creer un fichier vide dans le noeud courant
    -- parametres : src le noeud source, name le nom du fichier a creer
    -- post-condition : src possede une nouvelle metadonnee de type fichier de nom name
    procedure cmdTouch(src : in out ptr_node; name : in string);

    -- Creer un nouveau repertoire vide dans le noeud courant
    -- parametres : src le noeud source, name le nom du dossier a creer
    -- post-condition : src possede une nouvelle metadonnee de type dossier de nom name
    procedure cmdMkdir(src : in out ptr_node; name : in string);

    -- Affiche l'arborescence de l'arbre
    -- parametres : src le noeud source
    -- post-condition : affiche l'arborescence a partir du noeud source src
    procedure cmdTree(src : in ptr_node);

    -- Affiche le chemin absolu du noeud courant
    -- parametres : src le noeud source, str_path chaine de caractere
    -- post-condition : affiche le chemin absolu du noeud src a partir de la racine, et le sauvegarde si necessaire de str_path
    procedure cmdPwd(src : in ptr_node; str_path : out unbounded_string );   
    
    -- Affiche le chemin absolu du noeud courant
    -- parametres : src le noeud source
    -- post-condition : affiche le chemin absolu du noeud src a partir de la racine
    procedure cmdPwd(src : in ptr_node);   

    -- Supprimer recursivement un node
    -- parametres : dst le noeud destination
    -- exception : NODE_ERROR si dst est la racine de l'arbre
    -- post-condition : supprime toutes les metadonnees qui se trouvent dans le noeud dst
    procedure cmdRmRecursive(dst : in ptr_node); 

    -- Supprimer un fichier
    -- parametres : dst le noeud destination
    -- exception : NODE_ERROR si dst est un dossier
    -- post-condition : supprime le fichier dst 
    procedure cmdRm(dst : in ptr_node);

    -- Supprime le ficher en fonction de son nom
    -- parametres : src le noeud source, name le noeud du fichier a supprimer
    -- exception : NODE_ERROR si dst est un dossier
    -- post-condition : supprime le fichier de nom name a partir du noeud src
    procedure cmdRm(src : in ptr_node; name : in string);

    -- Naviguer du noeud courant vers le noeud destination
    -- parametres : src le noeud courant, dst le noeud destination
    -- exception : NODE_ERROR si dst est null ou dst un fichier
    -- post-condition : le noeud courant src se trouve sur le noeud dst
    procedure cmdCd(src : in out ptr_node; dst : in ptr_node);

    -- Naviguer du noeud courant vers le chemin de destination
    -- parametres : src le noeud courant, dst le chemin de destination
    -- exception : NODE_ERROR si dst est null ou dst un fichier
    -- post-condition : le noeud courant de l'arbre pointe par src devient  le noeud indique par dst
    procedure cmdCd(src : in out ptr_node; dst : in string);

    -- Copie les noeuds src vers dst
    -- parametres : src le noeud courant, dst le noeud destination
    -- exception : NODE_ERROR si dst est null ou dst un fichier
    -- post-condition : la metadonnee de src est copie dans le dossier dst
    procedure cmdCp(src : in ptr_node; dst : in out ptr_node);

    -- Copie les noeuds src vers dst a partir du noeud current
    -- parametres : current le noeud courant de l'arbre, src le chemin de la metadonnee a copie, dst le chemin de destination
    -- exception : NODE_ERROR si dst est null ou dst un fichier
    -- post-condition : copie la metadonnee src vers le noeud de destination dst a partir du noeud current 
    procedure cmdCp(current : in ptr_node; src : in string; dst : in string);

    -- Deplace le noeud src vers le noeud destination
    -- parametres : src le noeud courant, dst le noeud destination
    -- exception : NODE_ERROR si dst est null ou dst un fichier ou src la racine de l'arbre
    -- post-condition : la metadonnee de src est deplace dans le dossier dst et src.parent est dst
    procedure cmdMv(src : in out ptr_node; dst : in out ptr_node);

    -- Deplace le noeud src vers le noeud destination a partir d'une chaine de caractere
    -- parametres : current le noeud courant de l'arbre, src le chemin de la metadonnee a deplacer, dst le chemin de destination
    -- exception : NODE_ERROR si dst est null ou dst un fichier
    -- post-condition : la metadonnee de src est deplace dans le dossier dst depuis le noeud current et src.parent = dst
    procedure cmdMv(current : in ptr_node; src : in string; dst : in string);

    -- Creer un fichier tar a partir d'un noeud dossier source
    -- parametres : src le noeud source
    -- post-condition : src possede un nouveau fichier et ce fichier a le nom src.name
    procedure cmdTar(src : in out ptr_node);
    
    -- Creer un fichier tar a partir d'un chemin
    -- parametres : src le noeud source, path le chemin du dossier a tar
    -- post-condition : tar un dossier s trouvant au chemin path depuis le noeud source src
    procedure cmdTar(src : in out ptr_node; path : in string);
   
Private
    -- [Private] Creer une metadonnee selon la procedure de creation
    -- parametres : src le noeud source, name nom de la metadonnee, rights droits d'acces, proc pointeur de procedure de type pr_ptr_create
    -- post-condition : metadata correctement cree selon la procedure passe en parametre nomme name et ayant les bons droits rights
    procedure createMetadata(src : in out ptr_node; name : in string; rights : in string; proc : in pr_ptr_create);

    -- [Private] Affiche les metadonnee contenu dans un noeud selon la fonction d'affichage
    -- parametres : src le noeud source, sep string, func pointeur de fonction de type f_ptr_print
    -- post-condition : metadata correctement afficher selon la function passe en parametre func
    procedure printMetadata(src : in ptr_node; sep : in string; func : in f_ptr_print);

    -- [Private] Affecte la valeur dst a src
    -- parametres : src le noeud source, dst le noeud destination
    -- post-condition : src = dst
    procedure allocation(src : in out ptr_node; dst : in ptr_node);

    -- [Private] Recherche le noeud qui correspond au path
    -- parametres : src le noeud source, l_path liste chainee de string, proc pointeur de procedure pr_ptr_find
    -- post-condition : src pointe correctement sur le chemin indique par l_path selon la procedure de recherche/parcours
    procedure find(src: in out ptr_node; l_path : in out list_string.ptr_cell; proc : in pr_ptr_find );

    -- [Private] Recherche le noeud qui correspond au path
    -- parametres : src le noeud source, path le chemin, proc pointeur de procedure pr_ptr_find
    -- post-condition : src pointe correctement sur le chemin indique par path selon la procedure de recherche/parcours
    procedure find(src: in out ptr_node; path : in string; proc : in pr_ptr_find);

    

end;
