with ada.text_io, ada.integer_text_io;
use ada.text_io, ada.integer_text_io;

package adatest is

    TEST_FAILED : exception; -- exception quand un test echoue

    -- Affiche le nom de la methode a tester
    -- parametres : name nom de la methode
    -- post-condition : affiche le nom "### TEST <nomMethode> ###"
    procedure testname(name : in string);

    -- Affiche OK si le resultat correcte KO sinon
    -- parametres : res entier
    -- post-condition : KO si res=1 OK sinon
    procedure testresult(res : in integer);

    -- Affiche le cas d'une methode a tester
    -- parametres : name nom du cas
    -- post-condition : affiche "- <casDeTest> : "
    procedure testcase(name : in string);

end;
