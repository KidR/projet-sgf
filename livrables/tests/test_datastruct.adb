with ada.text_io, ada.integer_text_io, ada.calendar, ada.calendar.formatting;
use ada.text_io, ada.integer_text_io, ada.calendar, ada.calendar.formatting;

with datastruct, adatest;
use datastruct, adatest;

procedure test_datastruct is

    procedure test_setAccessRights is
        rights : t_access_rights;
    begin
        testname("SET ACCESS RIGHT");
        -- cas 1 : rights possede les droit classiques
        testcase("rights are 755");
        setAccessRights(rights,"755");
        if getOwner(rights) /= 7 and getGroup(rights) /= 5 and getOther(rights) /= 5 then
            raise TEST_FAILED;
        end if;
        printAccessRights(rights);
        put(" ");
        testresult(1);
        -- cas 2 : rights possede aucun droit
        testcase("rights do not have any access");
        setAccessRights(rights,"000");
        if getOwner(rights) /= 0 and getGroup(rights) /= 0 and getOther(rights) /= 0 then
            raise TEST_FAILED;
        end if;
        printAccessRights(rights);
        put(" ");
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_setAccessRights;

    procedure test_getAccessRights is
        rights : t_access_rights;
    begin
        testname("GET ACCESS RIGHTS");
        -- cas 1 : rights possede les droit 510
        testcase("rights are 510");
        setAccessRights(rights,"510");
        if getAccessRights(rights) /= "510" then
            raise TEST_FAILED;
        end if;
        printAccessRights(rights);
        put(" ");
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_getAccessRights;

    procedure test_setNowDate is
        d : t_date;
        now : time := clock;
    begin
        testname("SET NOW DATE");
        -- cas 1 : date d'aujourd'hui correspond
        testcase("today date is correct");
        setNowDate(d);
        if d.year /= ada.calendar.Year(now) or d.month /= ada.calendar.month(now) or d.day /= ada.calendar.Day(now) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_setNowDate;

    procedure test_timeString is
        d : t_date;
        now : time := clock;
    begin
        testname("TIME STRING");
        -- cas 1 : retourne le bon format de la date d'aujourd'hui
        testcase("today date's format AAAA-MM-DD HH:MM is correct");
        setNowDate(d);
        if timeString(d) /= Image(Date => now, Time_Zone => 60)(1..16) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_timeString;

begin
    test_setAccessRights;
    test_getAccessRights;
    test_setNowDate;
    test_timeString;
end;