with ada.text_io, ada.integer_text_io, ada.strings.unbounded;
use ada.text_io, ada.integer_text_io, ada.strings.unbounded;

with adatest, metadata, rshell;
use adatest, metadata, rshell;

with tree;
use rshell.tree_metadata;

procedure Test_rshell is

    root : ptr_node := new t_node;
    home : ptr_node := new t_node;
    etc : ptr_node := new t_node;
    docs : ptr_node := new t_node;
    desktop : ptr_node := new t_node;
    vim : ptr_node := new t_node;
    readme : ptr_node := new t_node;
    musics : ptr_node := new t_node;

    -- initialisation du jeu de donnee
    procedure setUp is
        
    begin
        initTree(root);
        -- definition des types de noeuds
        createDirectory(root.all.value,"/");
        createDirectory(home.all.value,"home");
        createDirectory(etc.all.value,"etc");
        createDirectory(docs.all.value,"docs");
        createDirectory(desktop.all.value,"desktop");
        createDirectory(musics.all.value,"musics");
        createDirectory(vim.all.value,"vim");
        createFile(readme.all.value,"readme");
        -- construction de l'arborescence
        addChild(root,home);
        addChild(root,etc);
        addChild(etc,vim);
        addChild(home,docs);
        addChild(home,desktop);
        addChild(home,musics);
        addChild(docs,readme);
    end setUp;

    -- remise a zero du jeu de donnee
    procedure tearDown is
        
    begin
        root := new t_node;
        home := new t_node;
        etc := new t_node;
        docs := new t_node;
        desktop := new t_node;
        vim := new t_node;
        readme := new t_node;
        musics := new t_node;
    end tearDown;

    procedure test_cmdLs is 

    begin
        setUp;
        testname("CMD LS");
        -- cas 1 : ls sur root
        testcase("ls on root");
        cmdLs(root);
        testresult(1);
        -- cas 2 : ls sur docs
        testcase("ls on home");
        cmdLs(home);
        testresult(1);
        -- cas 3 : ls sur un noeud contenant fichier 
        testcase("ls on file (exception expected)");
        begin
            cmdLs(readme);
            raise TEST_FAILED;
        exception
            when NODE_ERROR => testresult(1);
        end;
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_cmdLs;

    procedure test_cmdLsLong is 

    begin
        setUp;
        testname("CMD LS LONG");
        -- cas 1 : ls sur root
        testcase("ls -l on root");
        new_line;
        cmdLsLong(root);
        testresult(1);
        -- cas 2 : ls sur docs
        testcase("ls -l on home");
        new_line;
        cmdLsLong(home);
        testresult(1);
        -- cas 3 : ls sur un noeud contenant fichier 
        testcase("ls -l on file (exception expected)");
        begin
            cmdLsLong(readme);
            raise TEST_FAILED;
        exception
            when NODE_ERROR => testresult(1);
        end;
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_cmdLsLong;

    procedure test_cmdTouch is

    begin
        setUp;
        testname("CMD TOUCH");
        -- cas 1 : touch sur root
        testcase("touch on root");
        cmdTouch(root,"titouan");
        cmdLs(root);
        testresult(1);
        -- cas 2 : touch sur home
        testcase("touch on home");
        cmdTouch(root,"home/quema");
        cmdLs(home);
        testresult(1);
        -- cas 3 : touch sur un noeud contenant fichier 
        testcase("touch on file (exception expected)");
        begin
            cmdTouch(readme,"fail");
            raise TEST_FAILED;
        exception
            when NODE_ERROR => testresult(1);
        end;
        -- cas 4 : touch avec un nom errone
        testcase("touch with a wrong filename (exception expected)");
        begin
            cmdTouch(root,"my\file");
            raise TEST_FAILED;
        exception
            when metadata.NAME_ERROR => testresult(1);
        end;
        -- cas 5 : touch avec un nom deja existant
        testcase("touch with a name already taken (exception expected)");
        begin
            cmdMkdir(docs,"readme");
            raise TEST_FAILED;
        exception
            when metadata.NAME_ERROR => testresult(1);
        end;
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_cmdTouch;

    procedure test_cmdMkdir is

    begin
        setUp;
        testname("CMD MKDIR");
        -- cas 1 : touch sur root
        testcase("mkdir on root");
        cmdMkdir(root,"opt");
        cmdLs(root);
        testresult(1);
        -- cas 2 : touch sur home
        testcase("mkdir on home");
        cmdMkdir(root,"/home/movies");
        cmdLs(home);
        testresult(1);
        -- cas 3 : touch sur un noeud contenant fichier 
        testcase("mkdir on file (exception expected)");
        begin
            cmdMkdir(readme,"fail");
            raise TEST_FAILED;
        exception
            when NODE_ERROR => testresult(1);
        end;
        -- cas 4 : mkdir avec un nom errone
        testcase("mkdir with a wrong name (exception expected)");
        begin
            cmdMkdir(root,"my\dir");
            raise TEST_FAILED;
        exception
            when metadata.NAME_ERROR => testresult(1);
        end;
        -- cas 5 : mkdir avec un nom deja existant
        testcase("mkdir with a name already taken (exception expected)");
        begin
            cmdMkdir(etc,"vim");
            raise TEST_FAILED;
        exception
            when metadata.NAME_ERROR => testresult(1);
        end;
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_cmdMkdir;

    procedure test_cmdTree is

    begin
        setUp;
        testname("CMD TREE");
        -- cas 1 : depuis la racine
        testcase("tree from root");
        cmdTree(root);
        -- cas 2 : depuis home
        testcase("tree from home");
        cmdTree(home);
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0); 
    end test_cmdTree;

    procedure test_cmdPwd is

    begin
        setUp;
        testname("CMD PWD");
        -- cas 1 : pwd depuis la racine
        testcase("pwd from root");
        cmdPwd(root);
        testresult(1);
        -- cas 2 : pwd depuis docs
        testcase("pwd from docs");
        cmdPwd(docs);
        testresult(1);
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0); 
    end test_cmdPwd;

    procedure test_cmdRmRecursive is

    begin
        setUp;
        testname("CMD RM RECURSIVE");
        -- cas 1 : supprimer home
        testcase("rm -rf home");
        cmdRmRecursive(home);
        if hasChild(root,home) then
            raise TEST_FAILED;
        end if;
        cmdLs(root);
        testresult(1);
        tearDown;
        -- cas 2 : supprimer docs
        setUp;
        testcase("rm -rf docs (contains file)");
        cmdRmRecursive(docs);
        if hasChild(home,docs) then
            raise TEST_FAILED;
        end if;
        cmdLs(home);
        testresult(1);
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0); 
    end test_cmdRmRecursive;

    procedure test_cmdRm is

    begin
        setUp;
        testname("CMD RM");
        -- cas 1 : supprimer readme
        testcase("rm home");
        cmdRm(readme);
        if hasChild(docs,readme) then
            raise TEST_FAILED;
        end if;
        cmdLs(docs);
        testresult(1);
        -- cas 2 : supprimer docs
        testcase("rm home (exception expected)");
        begin
            cmdRm(home);
            raise TEST_FAILED;
        exception
            when NODE_ERROR => 
                cmdLs(root);
                testresult(1);
        end;
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0); 
    end test_cmdRm;

    procedure test_cmdCd is
        current : ptr_node;
    begin
        setUp;
        testname("CMD CD");
        -- cas 1 : noeud courant a la racine
        testcase("current from root to docs");
        current := root;
        cmdCd(current,docs);
        if current /= docs then
            raise TEST_FAILED;
        end if;
        cmdLs(current);
        testresult(1);
        -- cas 2 : noeud courant sur musics vers racine
        testcase("current from musics to root");
        current := musics;
        cmdCd(current,root);
        if current /= root then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 3 : noeud courant vers /home/musics
        testcase("current from root to /home/musics");
        current := root;
        cmdCd(current,"/home/musics");
        if current /= musics then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 3 : noeud courant vers .. (parent)
        testcase("docs to parent with '..'");
        current := docs;
        cmdCd(current,"../desktop");
        if current /= desktop then
            raise TEST_FAILED;
        end if;
        
        testresult(1);
        -- cas 4 : noeud courant vers docs avec .
        testcase("root to desktop with '.'");
        current := root;
        cmdCd(current,"./home/desktop");
        if current /= desktop then
            raise TEST_FAILED;
        end if;
        testresult(1);
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_cmdCd;

    procedure test_splitPath is
        l_path : list_string.ptr_cell;
    begin
        testname("SPLIT PATH");
        -- cas 1 : decoupe root/home/docs
        testcase("split the path root/home/docs");
        splitPath(l_path,"root/home/docs");
        if list_string.getValue(l_path,1) /= "root" or
            list_string.getValue(l_path,2) /= "home" or
            list_string.getValue(l_path,3) /= "docs" then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : decoupe la chaine sans /
        testcase("split string without '/'");
        l_path := null;
        splitPath(l_path,"rootruut");
        if list_string.getValue(l_path,1) /= "rootruut" then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 3 : decoupe de la chaine avec / premier
        testcase("split string with '/' at the beginning");
        l_path := null;
        splitPath(l_path,"/etc/vim");
        if list_string.getValue(l_path,1) /= "" then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_splitPath;

    procedure test_cmdCp is
        tmp : ptr_node;
    begin
        setUp;
        testname("CMD CP");
        -- cas 1 : simple cp
        testcase("cp docs in etc/");
        cmdCp(docs,etc);
        removeChild(docs,readme);
        if not(hasChild(home,docs) and not hasChild(docs,readme)) then
            raise TEST_FAILED;
        end if;
        tmp := list_node.get(src => etc.all.child,pos => 2).all.current;
        if getName(tmp.all.value) /= "docs" and tmp = docs then
            raise TEST_FAILED;
        end if;
        testresult(1);
        tearDown;
        -- cas 2 : cp directories with path
        setUp;
        testcase("cp with path from docs to ../etc");
        cmdCp(home,"docs","../etc");
        removeChild(docs,readme);
        if not(hasChild(home,docs) and not hasChild(docs,readme)) then
            raise TEST_FAILED;
        end if;
        tmp := list_node.get(src => etc.all.child,pos => 2).all.current;
        if getName(tmp.all.value) /= "docs" and tmp = docs then
            raise TEST_FAILED;
        end if;
        testresult(1);
        tearDown;
        -- cas 3 : cp file to directory
        setUp;
        testcase("cp file readme to ../etc");
        cmdCp(home,"docs/readme","../etc");
        removeChild(docs,readme);
        if hasChild(docs,readme) then
            raise TEST_FAILED;
        end if;
        tmp := list_node.get(src => etc.all.child,pos => 2).all.current;
        if getName(tmp.all.value) /= "readme" and tmp = readme then
            raise TEST_FAILED;
        end if;
        testresult(1);
        tearDown;
        -- cas 5
        setUp;
        testcase("cp etc to file readme (exception expected)");
        begin
            cmdCp(etc,readme);
            raise TEST_FAILED;
        exception
            when NODE_ERROR => testresult(1);
        end;
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_cmdCp;

    procedure test_cmdMv is
    
    begin
        setUp;
        testname("CMD MV");
        -- cas 1 : simple mv
        testcase("mv docs in etc/");
        cmdMv(docs,etc);
        if hasChild(home,docs) or not hasChild(etc,docs) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : deplace la racine
        testcase("mv root / (exception expected)");
        begin
            cmdMv(root,docs);
            raise TEST_FAILED;
        exception 
            when NODE_ERROR => testresult(1);
        end;
        -- cas 3 : deplace etcs dans home
        testcase("mv etc into home with path");
        cmdMv(etc,home);
        if not hasChild(home,etc) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 4 : renommage de home par house
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_cmdMv;

    procedure test_cmdTar is
        tmp : ptr_node;
    begin
        setUp;
        testname("CMD TAR");
        -- cas 1 : simple tar
        testcase("tar home");
        cmdTar(home);
        tmp := getChild(home,"home.tar");
        if getSize(tmp.all.value) /= 33 then
            raise TEST_FAILED;
        end if;
        testresult(1);
        tearDown;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_cmdTar;

    


begin
    -- INTRO
    put_line("% TESTS ARE WORKING ON THIS BASE %");
    setUp;
    printTree(root);
    tearDown;
    new_line;
    -- TESTS
    test_cmdLs;
    test_cmdLsLong;
    test_cmdTouch;
    test_cmdMkdir;
    test_cmdTree;
    test_cmdPwd;
    test_cmdRmRecursive;
    test_cmdRm;
    test_cmdCd;
    test_splitPath;
    test_cmdCp;
    test_cmdMv;
    test_cmdTar;
end;

