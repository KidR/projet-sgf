with ada.text_io, ada.integer_text_io, ada.strings.unbounded, ada.calendar, ada.calendar;
use ada.text_io, ada.integer_text_io, ada.strings.unbounded, ada.calendar, ada.calendar;

with adatest, metadata, datastruct;
use adatest, metadata, datastruct;

procedure Test_metadata is

    procedure test_getName is
        obj : t_metadata;
        str : unbounded_string;
    begin
        testname("GET NAME");
        -- cas 1 : obj n'a pas de nom (exception)
        testcase("obj is not named (exception expected)");
        begin
            str := to_unbounded_string(getName(obj));
            raise TEST_FAILED;
        exception
            when metadata.NAME_ERROR => testresult(1);
        end;
        -- cas 2 : obj porte le nom Object
        testcase("obj is named 'Object'");
        setName(obj,"Object");
        if obj.name /= getName(obj) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_getName;

    procedure test_setName is
        obj : t_metadata;
    begin
        testname("SET NAME");
        -- cas 1 : obj n'a pas de nom (exception)
        testcase("obj will be 'object'");
        setName(obj,"object");
        if getName(obj) /= "object" then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_setName;

    procedure test_getSize is
        obj : t_metadata;
    begin
        testname("GET SIZE");
        -- cas 1 : obj a une taille de 0
        testcase("obj has not size yet");
        if getSize(obj) /= 0 then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : obj a une taille de 5 octets
        testcase("obj has a size of 5 bytes");
        obj.size := 5;
        if getSize(obj) /= 5 then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_getSize;

    procedure test_getRights is
        obj : t_metadata;
        rights : t_access_rights;
    begin
        testname("GET RIGHTS");
        -- cas 1 : obj n'a aucun droit d'acces
        testcase("obj has none of any rights");
        setAccessRights(obj.rights,"000");
        rights := getRights(obj);
        if not(getOwner(rights) = 0 and getGroup(rights) = 0 and getOther(rights) = 0) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : obj a le droit 752
        testcase("obj has the right 752");
        setAccessRights(obj.rights,"752");
        rights := getRights(obj);
        if not(getOwner(rights) = 7 and getGroup(rights) = 5 and getOther(rights) = 2) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_getRights;

    procedure test_setRights is
        obj : t_metadata;
        rights : t_access_rights;
    begin
        testname("GET RIGHTS");
        -- cas 1 : obj n'a aucun droit d'acces
        testcase("set obj none of any rights");
        setRights(obj,"000");
        rights := getRights(obj);
        if not(getOwner(rights) = 0 and getGroup(rights) = 0 and getOther(rights) = 0) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : obj a le droit 752
        testcase("set obj rights 752");
        setRights(obj,"752");
        rights := getRights(obj);
        if not(getOwner(rights) = 7 and getGroup(rights) = 5 and getOther(rights) = 2) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 3 : les droits ne respectent pas la taille de 3
        testcase("set obj rights with wrong length (exception expected)");
        begin
            setRights(obj,"10");
            raise TEST_FAILED;
        exception
            when RIGHTS_ERROR => testresult(1);
        end;
        -- cas 4 : les droits comportent des lettres (au lieu de valeur octale)
        testcase("set obj rights with wrong syntax (exception expected)");
        begin
            setRights(obj,"rwa");
            raise TEST_FAILED;
        exception
            when RIGHTS_ERROR => testresult(1);
        end;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_setRights;

    procedure test_createMetadata is
        obj : t_metadata;
    begin
        testname("CREATE METADATA");
        -- cas 1 : create file 'readme'
        testcase("create file 'readme.md'");
        createFile(obj, name => "readme.md", rights => "755");
        if obj.name /= "readme.md" or obj.directory or getAccessRights(getRights(obj)) /= "755" then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : create directory 'Docs'
        testcase("create dir 'Docs'");
        createDirectory(obj, name => "Docs", rights => "644");
        if getName(obj) /= "Docs" or not isDirectory(obj) or getAccessRights(getRights(obj)) /= "644" then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 3 : create a file with wrong name
        testcase("create file with illegal characters");
        createFile(obj, name => "my/file");
        if not checkNameError(getName(obj)) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_createMetadata;

begin
    test_getName;
    test_setName;
    test_getSize;
    test_getRights;
    test_setRights;
    test_createMetadata;
    --test_getCreationTime;
end;

