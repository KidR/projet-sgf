with ada.text_io, ada.integer_text_io, ada.strings.unbounded, ada.calendar, ada.calendar.formatting, ada.calendar.time_zones;
use ada.text_io, ada.integer_text_io, ada.strings.unbounded, ada.calendar, ada.calendar.formatting, ada.calendar.time_zones;

with datastruct, adatest, metadata;
use datastruct, adatest, metadata;


with tree;

procedure Test_tree is

    package tree_metadata is new tree(t_metadata,getName);
    use tree_metadata;

    procedure test_initTree is
        root : ptr_node := new t_node;
    begin
        testname("INIT TREE");
        -- cas 1 : init tree 
        testcase("Init a node as root of tree");
        initTree(root);
        if not (list_node.isEmpty(root.all.child) and root.all.parent = null) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_initTree;

    procedure test_setRoot is
        root : ptr_node := new t_node;
        n : ptr_node := new t_node;
    begin
        testname("SET ROOT");
        -- cas 1 : root a un parent avant le set
        testcase("'root' had a parent 'n' before setting as root");
        root.all.parent := n;
        tree_metadata.setRoot(root);
        if not tree_metadata.isRoot(root) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_setRoot;

    procedure test_isRoot is
        root : ptr_node := new t_node;
        n : ptr_node := new t_node;
    begin
        testname("IS ROOT");
        -- cas 1 : root est la racine (par init)
        testcase("'root' is root");
        if not isRoot(root) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : n est la racine donc root plus la racine
        testcase("'n' becomes the root");
        root.all.parent := n;
        if isRoot(root) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_isRoot;

    procedure test_hasChild is
        root : ptr_node := new t_node;
        child : ptr_node := new t_node;
        a : ptr_node := new t_node;
        b : ptr_node := new t_node;
    begin
        testname("HAS CHILD");
        -- cas 1 : root a pas d'enfant 
        testcase("'root' has no child");
        setRoot(root);
        if hasChild(root,child) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : root est le parent de child
        testcase("'root' is parent of 'child'");
        root.all.child := new list_node.t_cell'(child,null);
        child.all.parent := root;
        if not hasChild(root, child) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 3 : root contains child named 'etc'
        testcase("root has child named 'etc'");
        initTree(root);
        addChild(root,a);
        addChild(root,b);
        -- completition du jeu de donnees
        setName(root.all.value,"root");
        setName(a.all.value,"etc");
        setName(b.all.value,"opt");
        if not hasChild(root,"etc") then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_hasChild;

    procedure test_hasParent is
        parent : ptr_node := new t_node; 
        node :  ptr_node := new t_node;
        isParent : boolean := false;
    begin
        testname("HAS PARENT");
        -- cas 1 : node a pas de parent car node est root
        testcase("'node' does not have parent");
        setRoot(node);
        begin
            isParent := hasParent(node,parent);
            raise TEST_FAILED;
        exception
            when NODE_ERROR => testresult(1);
        end;
        -- cas 2 : node a un parent 'parent'
        testcase("'node' has a parent");
        node.all.parent := parent;
        if not hasParent(node,parent) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_hasParent;

    procedure test_addChild is 
        node : ptr_node := new t_node;
        child : ptr_node := new t_node;
        child1 : ptr_node := new t_node;
        child2 : ptr_node := new t_node;
        child3 : ptr_node := new t_node;
    begin
        testname("ADD CHILD");
        -- cas 1 : node a un enfant
        testcase("'node' is parent of 'child'");
        addChild(node,child);
        if not hasChild(node,child) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : node possede 3 enfants
        testcase("'node' has 3 child");
        addChild(node,child1);
        addChild(node,child2);
        if not( hasChild(node,child1) and hasChild(node,child2) and hasChild(node,child) ) 
        or hasChild(node,child3) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 3 : node ajoute un noeud deja enfant
        testcase("'node' already has 'child' (exception expected)");
        begin
            addChild(node,child);
            raise TEST_FAILED;
        exception
            when NODE_ERROR => testresult(1);
        end;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_addChild;
    
    procedure test_removeChild is 
        node : ptr_node := new t_node;
        child : ptr_node := new t_node;
        child1 : ptr_node := new t_node;
        child2 : ptr_node := new t_node;
        child3 : ptr_node := new t_node;
    begin
        testname("REMOVE CHILD");
        -- cas 1 : node a pas d'enfant
        testcase("'node' has no 'child'");
        begin
            removeChild(node,child);
            raise TEST_FAILED;
        exception
            when list_node.CELL_ERROR => testresult(1);
        end;
        -- cas 2 : suppr. le 1er enfant
        testcase("remove the first child");
        addChild(node,child);
        addChild(node,child1);
        addChild(node,child2);
        addChild(node,child3);
        removeChild(node,child);
        if hasChild(node,child) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 3 : suppr. un enfant au milieu
        testcase("remove a child from the middle of the list");
        removeChild(node,child2);
        if hasChild(node,child2) or not hasChild(node,child3) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 4 : suppr. le dernier enfant
        testcase("remove the last child");
        removeChild(node,child3);
        if hasChild(node,child3) or not hasChild(node,child1) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_removeChild;

    procedure test_printTree is
        root : ptr_node := new t_node;
        a : ptr_node := new t_node;
        b : ptr_node := new t_node;
        a1 : ptr_node := new t_node;
        a11 : ptr_node := new t_node;
        a2 : ptr_node := new t_node;
        a3 : ptr_node := new t_node;
        a4 : ptr_node := new t_node;
        b1 : ptr_node := new t_node;
    begin
        testname("PRINT TREE");
        testcase("Printing a tree");
        -- construction de l'abre
        initTree(root);
        addChild(root,a);
        addChild(root,b);
        addChild(a,a1);
        addChild(a,a2);
        addChild(a,a3);
        addChild(a,a4);
        addChild(a1,a11);
        addChild(b,b1);
        -- completition du jeu de donnees
        setName(root.all.value,"root");
        setName(a.all.value,"a");
        setName(b.all.value,"b");
        setName(a1.all.value,"a1");
        setName(a2.all.value,"a2");
        setName(a3.all.value,"a3");
        setName(a4.all.value,"a4");
        setName(b1.all.value,"b1");
        setName(a11.all.value,"a11");
        new_line;
        printTree(root);
        testresult(1);
        new_line;
    end test_printTree;

    procedure test_getPath is
        root : ptr_node := new t_node;
        a : ptr_node := new t_node;
        b : ptr_node := new t_node;
        a1 : ptr_node := new t_node;
        a2 : ptr_node := new t_node;
        b1 : ptr_node := new t_node;
        str : unbounded_string;
    begin
        testname("GET PATH");
        -- construction de l'abre
        initTree(root);
        addChild(root,a);
        addChild(root,b);
        addChild(a,a1);
        addChild(a,a2);
        addChild(b,b1);
        -- completition du jeu de donnees
        setName(root.all.value,"root");
        setName(a.all.value,"a");
        setName(b.all.value,"b");
        setName(a1.all.value,"a1");
        setName(a2.all.value,"a2");
        setName(b1.all.value,"b1");
        -- cas 1 : get a1 from root
        testcase("get path of a1 from root");
        str := to_unbounded_string(getPath(root,a1));
        put(to_string(str));
        put(" ");
        if to_string(str) /= "root/a/a1/" then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : get b1 from b
        testcase("get path of b1 from b");
        str := to_unbounded_string(getPath(b,b1));
        put(to_string(str));
        put(" ");
        if to_string(str) /= "b/b1/" then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 3 : get a2 from b1 (error)
        testcase("get path of a2 from b1 (exception expected)");
        begin
            str := to_unbounded_string(getPath(a2,b1));
        exception
            when NODE_ERROR => testresult(1);
        end;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_getPath;

    procedure test_getRoot is
        tmp : ptr_node := new t_node;
        root : ptr_node := new t_node;
        a : ptr_node := new t_node;
        b : ptr_node := new t_node;
        a1 : ptr_node := new t_node;
        a2 : ptr_node := new t_node;
        b1 : ptr_node := new t_node;
    begin
        testname("GET ROOT");
        -- construction de l'arbre
        initTree(root);
        addChild(root,a);
        addChild(root,b);
        addChild(a,a1);
        addChild(a,a2);
        addChild(b,b1);
        -- completition du jeu de donnees
        setName(root.all.value,"root");
        setName(a.all.value,"a");
        setName(b.all.value,"b");
        setName(a1.all.value,"a1");
        setName(a2.all.value,"a2");
        setName(b1.all.value,"b1");
        -- cas 1 : get root depuis lui meme
        testcase("get root from root");
        tmp := getRoot(root);
        if not isRoot(tmp) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : get root depuis le bas de l'arbre
        testcase("get root from b1");
        tmp := getRoot(b1);
        if not isRoot(tmp) then
            raise TEST_FAILED;
        end if;
        testresult(1);
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_getRoot;

    procedure test_getChild is
        tmp : ptr_node;
        root : ptr_node := new t_node;
        a : ptr_node := new t_node;
        b : ptr_node := new t_node;
        a1 : ptr_node := new t_node;
        a2 : ptr_node := new t_node;
    begin
        testname("GET CHILD");
        -- construction de l'arbre
        initTree(root);
        addChild(root,a);
        addChild(root,b);
        addChild(a,a1);
        addChild(a,a2);
        -- completition du jeu de donnees
        setName(root.all.value,"root");
        setName(a.all.value,"a");
        setName(b.all.value,"b");
        setName(a1.all.value,"a1");
        setName(a2.all.value,"a2");
        -- cas 1 : renvoie le noeud a2 depuis son nom et parent
        testcase("get a2 from a");
        tmp := getChild(a,"a2");
        if tmp /= a2 then
            raise TEST_FAILED;
        end if;
        testresult(1);
        -- cas 2 : getChild alors que pas d'enfant
        testcase("get b1 from b (exception expected)");
        begin
            tmp := getChild(b,"b1");
            raise TEST_FAILED;
        exception
            when NODE_ERROR => testresult(1);
        end;
        new_line;
    exception
        when TEST_FAILED => testresult(0);
    end test_getChild;

    -- VARIABLES
    now : time := clock;

begin
    put_line("% Tests started : "&Image(Date => now, Time_Zone => 60));
    test_initTree;
    test_isroot;
    test_setroot;
    test_hasChild;
    test_hasParent;
    test_addChild;
    test_removeChild;
    test_printTree;
    test_getPath;
    test_getRoot;
    test_getChild;
end;

