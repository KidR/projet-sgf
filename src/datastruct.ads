with ada.text_io, ada.integer_text_io,  ada.calendar, ada.calendar.formatting, ada.strings.unbounded;
use ada.text_io, ada.integer_text_io,  ada.calendar, ada.calendar.formatting, ada.strings.unbounded;

package datastruct is

    RIGHTS_ERROR : exception; -- exception sur les droits d'acces

    -- caracterisation des droits d'acces par valeur octal (de 0 a 7)
    type t_access_rights is record 
        owner : integer; -- droit du groupe owner
        group : integer; -- droit du groupe group
        other : integer; -- droit du groupe other
    end record;

    -- caracterisation de la data de creation
    type t_date is record
        year : year_number; -- annee de creation
        month : month_number; -- mois de creation
        day : day_number; -- jour de creation
        second : day_duration; -- secondes de creation
    end record;

    -- Retourne les droits d'acces du groupe Owner sous forme octal
    -- parametres : src droits d'acces
    -- retour : entier
    -- post-condition : entier de 0 a 7
    function getOwner(src : in t_access_rights) return integer;

    -- Retourne les droits d'acces du groupe Group sous forme octal
    -- parametres : src droits d'acces
    -- retour : entier
    -- post-condition : entier de 0 a 7
    function getGroup(src : in t_access_rights) return integer;
    
    -- Retourne les droits d'acces du groupe Other sous forme octal
    -- parametres : src droits d'acces
    -- retour : entier
    -- post-condition : entier de 0 a 7
    function getOther(src : in t_access_rights) return integer;

    -- Retourne les droits d'acces sous forme octal string
    -- parametres : src droits d'acces
    -- retour : string
    -- post-condition : chaine sous forme "xxx" avec 0 <= x <=7
    function getAccessRights(src : in t_access_rights) return string;

    -- Modifie les droits d'accès src par les nouveaux droits d'accès value
    -- parametres : src droits d'acces, value valeur octale en string
    -- exception : RIGHT_ERROR si < 0 ou > 7
    -- post-condition : src.owner = value(1) et src.group = value(2) et src.other = value(3)
    procedure setAccessRights(src : in out t_access_rights; value : in String);

    -- Renvoie une chaine de caractere representant les droits d'acces rwx
    -- parametres : src droits d'acces
    -- retour : string droit d'acces rwx
    function image(src : t_access_rights) return string;

    -- Affiche les droits d'accès src en -rwx-
    -- parametres : scr droits d'acces
    -- post-condition : affiche les droits d'acces en rwx
    procedure printAccessRights(src : in t_access_rights);

    -- Renvoie la date sous le format AAAA-MM-DD HH:MM
    -- parametres : src la date de creation
    -- retour : string de la date 
    -- post-condition : renvoie une chaine de la forme AAAA-MM-DD HH:MM
    function timeString(src : in t_date) return string;

    -- Definie la date d'aujourd'hui
    -- parametres : src la date de creation
    -- post-condition : src prend la date correspondant au lancement de la procedure
    procedure setNowDate(src : in out t_date);

end;
