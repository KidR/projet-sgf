with ada.text_io, ada.integer_text_io, ada.calendar, ada.calendar.formatting, ada.calendar.time_zones;
use ada.text_io, ada.integer_text_io, ada.calendar, ada.calendar.formatting, ada.calendar.time_zones;

with datastruct;
use datastruct;

with ada.strings.unbounded;
use ada.strings.unbounded;

package metadata is

    CHARACTER_MAX : integer := 32; -- taille limite d'un fichier
    DEFAULT_FILE_SIZE : integer := 3; -- 3 Ko
    DEFAULT_DIR_SIZE : integer := 10; -- 10 Ko
    DEFAULT_FILE_RIGHTS : string := "755"; -- rwxr-xr-x
    DEFAULT_DIR_RIGHTS : string := "644"; -- rw-r--r--
    
    METADATA_ERROR, NAME_ERROR : exception;

    -- caracterisation des metadonnees
    type t_metadata is record
        name : unbounded_string; -- nom de la metadonnee
        directory : boolean; -- repertoire ou fichier
        rights : t_access_rights; -- droits d'acces
        creation_time : t_date; -- date de creation
        size : integer := 0; -- octet
    end record;

    -- Retourne le nom de la donnée
    -- parametres : obj la metadonnee
    -- exception : NAME_ERROR si nom vide
    function getName(obj : in t_metadata) return string;

    -- Renomme la donnée
    -- parametres : obj la metadonnee, name le nom du fichier
    -- exception : NAME_ERROR si taille du nom incorrecte
    -- post-condition : obj.name = name
    procedure setName(obj : in out t_metadata; name : in string);

    -- Retourne la taille de la donnée
    -- parametres : obj la metadonnee
    -- retour : entier correspondant a la taille de la metadonnee
    function getSize(obj : in t_metadata) return integer;

    -- Modifie la taille de la donnee
    -- parametres : obj la metadonnee, size la taille du fichier
    -- post-condition : obj.size = size
    procedure setSize(obj : in out t_metadata; size : in integer);

    -- Retourne les droits d'accès de la donnée
    -- parametres : obj la metadonnee
    -- retour : t_access_rights enregistrement des droits
    function getRights(obj : in t_metadata) return t_access_rights;

    -- Attribut les droits d'accès d'une donnée 
    -- parametres : obj la metadonnee, rights les droits d'acces
    -- post-condition : obj.rights = rights;
    procedure setRights(obj : in out t_metadata; rights : in t_access_rights);

    -- Attribut les droits d'accès d'une donnée 
    -- parametres : obj la metadonnee, rights les droits d'acces sous forme octale
    -- post-condition : obj.rights = rights
    procedure setRights(obj : in out t_metadata; rights : in string);

    -- Retourne la date de création de la donnée
    -- parametres : obj la metadonnee
    -- retour : t_date la date de creation de la metadonnee
    function getCreationTime(obj : in t_metadata) return t_date;

    -- Retourne vrai si la donnée est un dossier faux sinon
    -- parametres : obj la metadonnee
    -- retour : boolean vrai si c'est un repertoire faux sinon
    function isDirectory(obj : in t_metadata) return boolean;

    -- Creer un répertoire en lui renseignant son nom, son type et ses droits
    -- parametres : obj la metadonnee, name son nom, rights les droits d'acces
    -- post-condition : obj est initialise en tant que dossier
    procedure createDirectory(obj : out t_metadata; name : in string; rights : in string := DEFAULT_DIR_RIGHTS);

    -- Creer un fichier en lui renseignant son nom, son type et ses droits
    -- parametres : obj la metadonnee, name son nom, rights les droits d'acces
    -- post-condition : obj est initialise en tant que fichier
    procedure createFile(obj : out t_metadata; name : in string; rights : in string := DEFAULT_FILE_RIGHTS);

    -- Verifie que le nom des metadonnees est correcte
    -- parametres : name le nom d'une metadonnee
    -- retour : boolean vrai si le nom est correcte faux sinon
    -- pre-condition : name ne contient pas "/" ou "\" dans son nom
    function checkNameError(name : in string) return boolean;

    -- Renvoie sous chaine de caractere la representation d'une metadonnee
    -- parametres : obj la metadonnee
    -- retour : string
    -- post-condition : affiche la representation de la metadonnee
    function image(obj : in t_metadata) return string;

private

    -- Creer une donnée en lui renseignant son nom, son type et ses droits
    -- parametres : obj la metadonnee, name son nom, directory vrai si repertoire sinon fichier, rights les droits d'acces
    -- post-condition : tous les champs de la metadonnee sont initialises
    procedure createMetadata(obj : out t_metadata; name : in string; directory : in boolean; rights : in string);


end;
