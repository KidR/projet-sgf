with ada.text_io, ada.integer_text_io;
use ada.text_io, ada.integer_text_io;

package body list is

    function isEmpty(src : in ptr_cell) return boolean is

    begin
        return src = null;
    end isEmpty;

    procedure add(src : in out ptr_cell; e: in T) is
        tmp : ptr_cell;

        -- R1 TANT QUE la cellule suivante ne soit pas vide FAIRE
            -- R2 TANT QUE celluleCourante non vide /= e FAIRE
        -- R1 Prendre la cellule suivante
            -- R2 celluleCourante <- celluleSuivante
        -- R1 FIN TANT QUE
        -- R1 SI la cellule courante vide ALORS
            -- R2 SI celluleCourante = null FAIRE
        -- R1 Creer une nouvelle cellule dans la liste chainee
            -- celluleCourante.curr <- nouvelleCellule(e)
        -- FIN SI

    begin
        if src = null then
            src := new t_cell'(e,null);
        else
            tmp := src;
            while tmp.all.next /= null loop
                tmp := tmp.all.next;
            end loop;
            tmp.all.next := new t_cell'(e,null);
        end if;
    end add;

    procedure remove(src : in out ptr_cell; e: in T) is
        tmp : ptr_cell;

        -- R1 TANT QUE la cellule suivante de la cellule courante ne contient pas l'element e et qu'elle ne soit pas vide FAIRE
            -- R2 TANT QUE celluleCourante non vide ET ENSUITE celluleCourante.suivante.curr /= e FAIRE
        -- R1 Prendre la cellule suivante
            -- R2 celluleCourante <- celluleSuivante
        -- R1 FIN TANT QUE
        -- R1 SI la cellule suivante de la cellule courante contient l'element e ALORS
            -- R2 SI celluleCourante.suivante.curr = e FAIRE
        -- R1 Supprimer cette cellule de la liste chainee
            -- celluleCourante.suivante = celluleCourante.suivante.suivante
        -- FIN SI

    begin
        if src = null then
            raise CELL_ERROR;
        end if;
        if src.all.current = e then
            src := src.all.next;
        else                     
            tmp := src;
            while tmp.all.next /= null and then tmp.all.next.all.current /= e loop
                tmp := tmp.all.next;
            end loop;
            if tmp.all.next.all.current = e then
                tmp.all.next := tmp.all.next.all.next;
            end if;
        end if;
    end remove;

    function get(src : in ptr_cell; e : in T) return ptr_cell is
        tmp : ptr_cell;

        -- R1 TANT QUE la cellule courante ne soit pas vide et que la cellule courante ne contient pas l'element e FAIRE
            -- R2 TANT QUE celluleCourante non vide ET ENSUITE celluleCourante.suivante.curr /= e FAIRE
        -- R1 Prendre la cellule suivante
            -- R2 celluleCourante <- celluleSuivante
        -- R1 FIN TANT QUE
        -- R1 SI la cellule suivante de la cellule courante contient l'element e ALORS
            -- R2 SI celluleCourante.suivante.curr = e FAIRE
        -- R1 Retourner la cellule courante
            -- R2 Return celluleCourante
        -- FIN SI
    begin
        if src = null then
            raise CELL_ERROR;
        end if;
        tmp := src;
        while tmp /= null and then tmp.all.current /= e loop
            tmp := tmp.all.next;
        end loop;
        if tmp = null then
            raise CELL_ERROR with "Element you are looking for does not exist in list.";
        end if;
        return tmp;
    end get;

    function get(src : in ptr_cell; pos : in integer) return ptr_cell is
        tmp : ptr_cell;
        cpt : integer := 1;
    begin
        if src = null then
            raise CELL_ERROR;
        end if;
        if pos < 1 then
            raise CONSTRAINT_ERROR;
        end if;
        tmp := src;
        while tmp /= null and then cpt /= pos loop
            tmp := tmp.all.next;
            cpt := cpt + 1;
        end loop;
        return tmp;
    end get;

    function getLast(src : in ptr_cell) return ptr_cell is
        tmp : ptr_cell;
    begin
        if src = null then
            raise CELL_ERROR;
        end if;
        tmp := src;
        while tmp.all.next /= null loop
            tmp := tmp.all.next;
        end loop;
        return tmp;
    end getLast;

    function getValue(src : in ptr_cell; pos : in integer) return T is

    begin
        return get(src,pos).all.current;
    end getValue;

    procedure removeLast(src : in out ptr_cell) is
        tmp : ptr_cell;
    begin
        if src = null then
            raise CELL_ERROR;
        end if;
        tmp := src;
        while tmp.all.next /= null loop
            tmp := tmp.all.next;
        end loop;
        tmp := null;
    end removeLast;
    
    procedure removeFirst(src : in out ptr_cell) is

    begin
        src := src.all.next;
    end removeFirst;

    function pop(src : in out ptr_cell; pos : in integer := 1) return T is
        tmp : ptr_cell;
        value : T;
        cpt : integer := 0;

        -- R1 compteur <- 0
        -- R1 res <- null
        -- R1 TANT QUE la cellule courante ne soit pas vide et que le compteur de cellule soit inferieur stricte a la position donnee FAIRE
            -- R2 TANT QUE celluleCourante non vide ET ENSUITE compteur < position FAIRE
        -- R1 Prendre la cellule suivante et incrementer le compteur
            -- R2 celluleCourante <- celluleSuivante
            -- R2 compteur <- compteur + 1
        -- R1 FIN TANT QUE
        -- R1 Stocker la valeur de la cellule courante
            -- R2 res <- celluleCourante.curr
        -- R1 Supprimer la cellule courante
            -- R2 celluleCourante <- celluleCourante.next SI celluleCourante.next existe
        -- R1 Retourner la valeur de la cellule courante
            -- R2 Return res

    begin
        if src = null then
            raise CELL_ERROR;
        end if;
        if pos < 1 then
            raise CONSTRAINT_ERROR;
        end if;
        if pos = 1 then
            value := src.all.current;
            src:= src.all.next;
        else
            tmp := src;
            while tmp /= null and then cpt < pos loop
                tmp := tmp.all.next;
                cpt := cpt + 1;
            end loop;
            if tmp /= null and then tmp.all.next /= null then
                value := tmp.all.next.all.current;
                tmp := tmp.all.next.all.next;
            end if;
        end if;
        return value;
    end pop;

end;

