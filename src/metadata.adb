with ada.text_io, ada.integer_text_io;
use ada.text_io, ada.integer_text_io;

with ada.strings.unbounded, ada.strings.fixed;
use ada.strings.unbounded, ada.strings.fixed;

with metadata;
use metadata;

package body metadata is

    function getName(obj : in t_metadata) return string is

    begin
        if obj.name = to_unbounded_string("") then
            raise metadata.NAME_ERROR;
        end if;
        return to_string(obj.name);
    end getName;

    procedure setName(obj : in out t_metadata; name : in string) is

    begin
        if name'Length > CHARACTER_MAX or name'Length < 0 then
            raise metadata.NAME_ERROR;
        end if;
        obj.name := to_unbounded_string(name);
    end setName;

    function getSize(obj : in t_metadata) return integer is

    begin
        return obj.size;
    end getSize;

    procedure setSize(obj : in out t_metadata; size : in integer) is

    begin   
        obj.size := size;
    end setSize;

    function getRights(obj : in t_metadata) return t_access_rights is

    begin
        return obj.rights;
    end getRights;

    procedure setRights(obj : in out t_metadata; rights : in t_access_rights) is

    begin
        obj.rights := rights;
    end setRights;

    procedure setRights(obj : in out t_metadata; rights : in string) is

    begin
        setAccessRights(obj.rights,rights);
    end setRights;

    function getCreationTime(obj : in t_metadata) return t_date is

    begin
        return obj.creation_time;
    end getCreationTime;

    function isDirectory(obj : in t_metadata) return boolean is

    begin
        return obj.directory;
    end isDirectory;

    procedure createMetadata(obj : out t_metadata; name : in string; directory : in boolean; rights : in string) is
        now : time := clock;
    begin
        setName(obj,name);
        setRights(obj,rights);
        obj.directory := directory;
        setNowDate(obj.creation_time);
        if directory then
            obj.size := metadata.DEFAULT_DIR_SIZE;
        else
            obj.size := metadata.DEFAULT_FILE_SIZE;
        end if;
    end createMetadata;

    procedure createDirectory(obj : out t_metadata; name : in string; rights : in string := DEFAULT_DIR_RIGHTS) is

    begin
        createMetadata(obj,name,true,rights);
    end createDirectory;

    procedure createFile(obj : out t_metadata; name : in string; rights : in string := DEFAULT_FILE_RIGHTS) is

    begin
        createMetadata(obj,name,false,rights);
    end createFile;

    function checkNameError(name : in string) return boolean is
        
        function contains(str : in string; c : in string) return boolean is

        begin
            return index(str,c) > 0;
        end contains;

    begin
        return contains(name,"/") or contains(name,"\");
    end checkNameError;

    function image(obj : in t_metadata) return string is
        dir : String:= "-";
        user, group : string := "root";
    begin
        if isDirectory(obj) then
            dir := "d";
        end if;
        return dir & datastruct.image(getRights(obj)) & " " & user & " " & group & " " & getSize(obj)'Image & "B " & datastruct.timeString(getCreationTime(obj)) & " " & getName(obj);
    end image;

end;

