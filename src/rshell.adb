with ada.text_io, ada.integer_text_io;
use ada.text_io, ada.integer_text_io;

with Ada.Characters.Latin_1;
use Ada.Characters.Latin_1;

package body rshell is

    procedure splitString(res : out ptr_cell; str : in string; c : in string) is
        tmp : unbounded_string;
        j : integer;

    begin
        tmp := to_unbounded_string(str);
        loop
            j := Index(tmp,c);
            exit when j <= 0;
            add(res,unbounded_slice(tmp,1,j-1));
            tmp := unbounded_slice(tmp,j+1,length(tmp));
        end loop;
        if unbounded_slice(tmp,1,length(tmp)) /= "" then
            add(res,unbounded_slice(tmp,1,length(tmp)));
        end if;
    end splitString;

    procedure splitPath(res : out ptr_cell; str : in string) is
        tmp : unbounded_string;
    begin
        splitString(res,str,"/");
    end splitPath;

    procedure printMetadata(src : in ptr_node; sep : in string; func : in f_ptr_print) is
        l : list_node.ptr_cell;
    begin
        if not isDirectory(src.all.value) then
            raise NODE_ERROR with "must create a file into a directory.";
        end if;
        l := src.all.child;
        -- new_line;
        while not list_node.isEmpty(l) loop
            put(func.all(l.all.current.all.value));
            if sep = "\n" then
                new_line;
            else
                put(sep);
            end if;
            l := l.all.next;
        end loop;
    end printMetadata;

    procedure createMetadata(src : in out ptr_node; name : in string; rights : in string; proc : in pr_ptr_create) is
        node : ptr_node;
        data : t_metadata;
        l_path : list_string.ptr_cell;
    begin
        splitPath(l_path,name);
        while not list_string.isEmpty(l_path.all.next)  loop
            processCd(src,to_string(l_path.all.current));
            l_path := l_path.all.next;
        end loop;
        if not isDirectory(src.all.value) then
            raise NODE_ERROR with "must create a file into a directory.";
        end if;
        if checkNameError(to_string(l_path.all.current)) then
            raise metadata.NAME_ERROR with "Illegal characters.";
        end if;
        if hasChild(src,to_string(l_path.all.current)) then
            raise metadata.NAME_ERROR with to_string(l_path.all.current)&" already exists.";
        end if;
        node := new t_node'(src,null,data);
        proc.all(node.all.value,to_string(l_path.all.current),rights);
        addChild(src,node);
    end createMetadata;

    procedure processCd(src : in out ptr_node; path : in string) is

    begin
        if path = "" then
        -- absolute path
            src := getRoot(src);
        elsif path = SYMBOLE_CURRENT_DIR then
        -- begin with "./"
            null;
        elsif path = SYMBOLE_PARENT_DIR then
        -- begin with ".."
            cmdCd(src,src.all.parent);
        else
            src := getChild(src,path);
        end if;
    end processCd;

    procedure find(src: in out ptr_node; l_path : in out list_string.ptr_cell; proc : in pr_ptr_find ) is
        tmp : ptr_node;
    begin
        -- begin
        tmp := src;
        while not list_string.isEmpty(l_path) loop
            processCd(tmp,to_string(l_path.all.current));
            proc.all(src,tmp);
            l_path := l_path.all.next;
        end loop;
        -- end
    end find;

    procedure find(src: in out ptr_node; path : in string; proc : in pr_ptr_find ) is
        l_path : list_string.ptr_cell;
    begin
        -- begin
        splitPath(l_path,path);
        find(src,l_path,proc);
        -- end
    end find;

    procedure allocation(src : in out ptr_node; dst : in ptr_node) is

    begin
        if src /= null then
            src := dst;
        end if;
    end allocation;

    procedure cmdLs(src : in ptr_node) is 
        sep : string := "    "; -- 4x <space>
    begin
        printMetadata(src,sep,getName'access);
    end cmdLs;

    procedure cmdLsLong(src : in ptr_node) is
        sep : string := "\n";
    begin
        printMetadata(src,sep,metadata.image'access);
    end cmdLsLong;

    procedure cmdTouch(src : in out ptr_node; name : in string) is

    begin
        createMetadata(src,name,metadata.DEFAULT_FILE_RIGHTS,createFile'access);
    end cmdTouch;

    procedure cmdMkdir(src : in out ptr_node; name : in string) is

    begin
        createMetadata(src,name,metadata.DEFAULT_DIR_RIGHTS,createDirectory'access);
    end cmdMkdir;

    procedure cmdTree(src : in ptr_node) is

    begin
        new_line;
        printTree(src);
    end cmdTree;

    procedure cmdPwd(src : in ptr_node; str_path : out unbounded_string) is
        str : unbounded_string;
        root : ptr_node;
        lower_bound : integer := 1;
    begin
        root := getRoot(src);
        if root /= src then
            lower_bound := lower_bound + 1;
        end if;
        str := to_unbounded_string(getPath(root,src));
        str_path := to_unbounded_string(to_string(str)(lower_bound .. length(str)-1));
        put(to_string(str_path));
    end cmdPwd;

    procedure cmdPwd(src : in ptr_node) is
        str : unbounded_string;
    begin
        cmdPwd(src,str);
    end cmdPwd;

    procedure cmdRmRecursive(dst : in ptr_node) is

    begin
        if isRoot(dst) then
            raise NODE_ERROR with "Impossible to remove root.";
        end if;
        removeChild(dst.all.parent,dst); 
    end cmdRmRecursive;

    procedure cmdRm(dst : in ptr_node) is

    begin
        if isDirectory(dst.all.value) then
            raise NODE_ERROR with "Unable to remove a directory. Use rm -rf instead.";
        end if;
        removeChild(dst.all.parent,dst);
    end cmdRm;

    procedure cmdRm(src : in ptr_node; name : in string) is
        tmp : ptr_node;
    begin
        tmp := src;
        find(tmp,name,rshell.allocation'access);
        cmdRm(tmp);
    end cmdRm;

    procedure cmdCd(src : in out ptr_node; dst : in ptr_node) is
    
    begin
        if dst = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        if not isDirectory(dst.all.value) then
            raise NODE_ERROR with "must change directory into another one.";
        end if;
        src := dst;
    end cmdCd;

    procedure cmdCd(src : in out ptr_node; dst : in string) is

    begin
        find(src,dst,rshell.cmdCd'access);
    end cmdCd;

    procedure cmdCp(src : in ptr_node; dst : in out ptr_node) is
    begin
        if src = null or dst = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        if not isDirectory(dst.all.value) then
            raise NODE_ERROR with "must copy into a directory.";
        end if;
        addChild(dst, new t_node'(src.all.parent,src.all.child,src.all.value));
    end cmdCp;

    procedure cmdCp(current : in ptr_node; src : in string; dst : in string) is
        tmp_src : ptr_node;
        tmp_dst : ptr_node;
    begin
        if current = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        tmp_src := current;
        tmp_dst := current;
        -- begin
        find(tmp_src,src,rshell.allocation'access);
        -- end
        cmdCd(tmp_dst, dst);
        cmdCp(tmp_src, tmp_dst);
    end cmdCp;

    procedure cmdMv(src : in out ptr_node; dst : in out ptr_node) is
    begin
        if src = null or dst = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        if isRoot(src) then
            raise NODE_ERROR with "Impossible to move root (/).";
        end if;
        if not isDirectory(dst.all.value) then
            raise NODE_ERROR with "must move into a directory.";
        end if;
        if src /= dst then
            removeChild(src.all.parent ,src);
            addChild(dst,src);
        end if;
    end cmdMv;

    procedure cmdMv(current : in ptr_node; src : in string; dst : in string) is
        tmp_src : ptr_node;
        tmp_dst : ptr_node;
    begin
        if current = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        tmp_src := current;
        tmp_dst := current;
        -- begin
        find(tmp_src,src,rshell.allocation'access);
        -- end
        cmdCd(tmp_dst, dst);
        cmdMv(tmp_src, tmp_dst);
    end cmdMv;

    procedure cmdTar(src : in out ptr_node) is
        tmp : ptr_node;
        res : integer := 0;
        tar_ext : string := ".tar"; 

        procedure sum(src : in ptr_node; size : in out integer) is
            tmp_child : list_node.ptr_cell;
        begin
            size := size + getSize(src.all.value);
            tmp_child := src.all.child;
            while not list_node.isEmpty(tmp_child) loop
                sum(tmp_child.all.current,size);
                tmp_child := tmp_child.all.next;
            end loop;
        end sum;

    begin
        sum(src,res);
        res := res - getSize(src.all.value);
        cmdTouch(src,getName(src.all.value)&tar_ext);
        tmp := getChild(src,getName(src.all.value)&tar_ext);
        setSize(tmp.all.value,res);
    end cmdTar;

    procedure cmdTar(src : in out ptr_node; path : in string) is
        tmp_src : ptr_node;
    begin
        tmp_src := src;
        find(tmp_src,path,rshell.cmdCd'access);
        cmdTar(tmp_src);
    end cmdTar;

end;

