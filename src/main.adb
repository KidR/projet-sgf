with ada.text_io, ada.integer_text_io, ada.strings.unbounded, ada.strings.unbounded.text_io;
use ada.text_io, ada.integer_text_io, ada.strings.unbounded, ada.strings.unbounded.text_io;

with adatest, metadata, rshell;
use adatest, metadata, rshell;

with tree;
use rshell.tree_metadata;
use rshell.list_string;

procedure main is

    MAX_CHARACTERS : integer := 1024;

    root : ptr_node := new t_node;
    home : ptr_node := new t_node;
    etc : ptr_node := new t_node;
    docs : ptr_node := new t_node;
    desktop : ptr_node := new t_node;
    vim : ptr_node := new t_node;
    readme : ptr_node := new t_node;
    musics : ptr_node := new t_node;

    procedure setUp is
    begin    
        initTree(root);
        -- definition des types de noeuds
        createDirectory(root.all.value,"/");
        createDirectory(home.all.value,"home");
        createDirectory(etc.all.value,"etc");
        createDirectory(docs.all.value,"docs");
        createDirectory(desktop.all.value,"desktop");
        createDirectory(musics.all.value,"musics");
        createDirectory(vim.all.value,"vim");
        createFile(readme.all.value,"readme");
        -- construction de l'arborescence
        addChild(root,home);
        addChild(root,etc);
        addChild(etc,vim);
        addChild(home,docs);
        addChild(home,desktop);
        addChild(home,musics);
        addChild(docs,readme);
    end setUp;

    procedure tearDown is
    begin    
        root := new t_node;
        home := new t_node;
        etc := new t_node;
        docs := new t_node;
        desktop := new t_node;
        vim := new t_node;
        readme := new t_node;
        musics := new t_node;
    end tearDown;

    procedure printInput(curr : in ptr_node; current_path : out unbounded_string) is
    
    begin
        put("root@rshell:");
        cmdPwd(curr,current_path);
        put("$> ");
    end printInput;

    procedure processPath(src : in out ptr_node; path : in string) is
        l_path : list_string.ptr_cell;
    begin
        splitPath(l_path,path);
        while not list_string.isEmpty(l_path) loop
            processCd(src,to_string(l_path.all.current));
            l_path := l_path.all.next;
        end loop;
    end processPath;

    procedure mainMenu is

        procedure printCmds(curr : in ptr_node; current_path : out unbounded_string) is
            spaces : string := "     "; -- 5x spaces
        begin
            new_line;
            put_line("0- exit");
            put_line("1- ls");
            put_line("2- ls -l");
            put_line("3- tree");
            put_line("4- cd");
            put_line("5- pwd");
            put_line("6- touch");
            put_line("7- mkdir");
            put_line("8- tar");
            put_line("9- rm");
            put_line("10- rm -r");
            put_line("11- mv");
            put_line("12- cp");
            new_line;
            printInput(curr,current_path);
        end printCmds;

        function inputLine(str : in string) return unbounded_string is
            buf : unbounded_string;
        begin
            put(str);
            skip_line;
            buf := ada.strings.unbounded.text_io.get_line;
            return buf;
        end;


        choice : integer;
        curr : ptr_node;
        tmp_node : ptr_node;
        current_path : unbounded_string;
        str : unbounded_string;
        str_dst : unbounded_string;

    begin
        setUp;
        curr := root;
        loop
            printCmds(curr,current_path);
            get(choice);
            tmp_node := curr;
            case choice is
            when 1 =>
                str := inputLine("Path: ");
                processPath(tmp_node,to_string(str));
                cmdLs(tmp_node);
            when 2 =>
                str := inputLine("Path: ");
                processPath(tmp_node,to_string(str));
                cmdLsLong(tmp_node);
            when 3 =>
                str := inputLine("Path: ");
                processPath(tmp_node,to_string(str));
                cmdTree(tmp_node);
            when 4 =>
                begin
                    str := inputLine("Path: ");
                    cmdCd(curr,to_string(str));
                exception
                    when PATH_ERROR => put("***** Incorrect path.");
                end;
            when 5 =>
                cmdPwd(curr);
            when 6 =>
                str := inputLine("New filename: ");
                cmdTouch(tmp_node,to_string(str));
            when 7 =>
                str := inputLine("New directory's name: ");
                cmdMkdir(tmp_node,to_string(str));
            when 8 =>
                str := inputLine("Directory to tar: ");
                cmdTar(tmp_node,to_string(str));
            when 9 =>
                str := inputLine("Path of file to remove: ");
                cmdRm(tmp_node,to_string(str));
            when 10 =>
                str := inputLine("Path to remove: ");
                processPath(tmp_node,to_string(str));
                cmdRmRecursive(tmp_node);
            when 11 =>
            -- buffer problem with 2 inputLine in a row.
                str := inputLine("Move from: ");
                put("Move to: ");
                str_dst := ada.strings.unbounded.text_io.get_line;
                cmdMv(tmp_node,to_string(str),to_string(str_dst));
            when 12 =>
                str := inputLine("Copy from: ");
                put("Copy to: ");
                str_dst := ada.strings.unbounded.text_io.get_line;
                cmdCp(tmp_node,to_string(str),to_string(str_dst));
            when others => 
                null;
            end case;
            exit when choice = 0;
        end loop;
        tearDown;
    end mainMenu;

    procedure mainShell is

        type ptr_pr_cmdGoto is access procedure(node : in ptr_node);

        type ptr_pr_cmdUpdate is access procedure(node : in out ptr_node; name : in string);
        
        type ptr_pr_cmdDelete is access procedure(node : in ptr_node; name : in string);
        
        type ptr_pr_cmdMvCp is access procedure(node : in ptr_node; src : in string; dst : in string);

        procedure processGoto(curr : in out ptr_node; cmds : in out list_string.ptr_cell; proc : in ptr_pr_cmdGoto) is
            tmp_node : ptr_node := curr;
        begin
            if isEmpty(cmds) then
                proc.all(tmp_node);
            else
                while not isEmpty(cmds) loop
                    processPath(tmp_node,to_string(pop(cmds)));
                    proc.all(tmp_node);
                    tmp_node := curr;
                    new_line;
                end loop;
            end if;
        end processGoto;

        procedure processUpdate(curr : in out ptr_node; cmds : in out list_string.ptr_cell; proc : in ptr_pr_cmdUpdate) is
            tmp_node : ptr_node := curr;
        begin
            while not isEmpty(cmds) loop
                proc.all(tmp_node,to_string(pop(cmds)));
                tmp_node := curr;
            end loop;
        end processUpdate;

        procedure processUpdate(curr : in out ptr_node; cmds : in out list_string.ptr_cell; proc : in ptr_pr_cmdDelete) is
            tmp_node : ptr_node := curr;
        begin
            while not isEmpty(cmds) loop
                proc.all(tmp_node,to_string(pop(cmds)));
                tmp_node := curr;
            end loop;
        end processUpdate;

        procedure processUpdate(curr : in out ptr_node; cmds : in out list_string.ptr_cell; proc : in ptr_pr_cmdMvcp) is
            tmp_node : ptr_node := curr;
            src : unbounded_string;
            dst : unbounded_string; 
        begin
            src := pop(cmds);
            dst := pop(cmds);
            proc.all(tmp_node,to_string(src),to_string(dst));
            tmp_node := curr;
        end processUpdate;

        input : unbounded_string;
        curr : ptr_node;
        tmp_node : ptr_node;
        current_path : unbounded_string;
        cmds : list_string.ptr_cell;
        cmd_name : unbounded_string;
        tmp_str : unbounded_string;
    begin
        setUp;
        curr := root;
        loop
            printInput(curr,current_path);
            tmp_node := curr;
            input := ada.strings.unbounded.text_io.get_line;
            if input /= "" and input /= "exit" then
                splitString(cmds,to_string(input)," ");
                cmd_name := pop(cmds);
                case Cmd'Value(to_string(cmd_name)) is
                    when ls => 
                        processGoto(curr,cmds,cmdLs'access);
                    when ll =>
                        processGoto(curr,cmds,cmdLsLong'access);
                    when rshell.tree =>
                        processGoto(curr,cmds,cmdTree'access);
                    when cd =>
                        if isEmpty(cmds) then
                            cmdCd(curr,"/home");
                        else
                            cmdCd(curr,to_string(pop(cmds)));
                        end if;
                    when pwd =>
                        cmdPwd(curr);
                    when touch =>
                        processUpdate(curr,cmds,cmdTouch'access);
                    when mkdir =>
                        processUpdate(curr,cmds,cmdMkdir'access);
                    when tar =>
                        processUpdate(curr,cmds,cmdTar'access);
                    when rm =>
                        processUpdate(curr,cmds,cmdRm'access);
                    when rr =>
                        processGoto(curr,cmds,cmdRmRecursive'access);
                    when mv =>
                        processUpdate(curr, cmds, cmdMv'access);
                    when cp =>
                        processUpdate(curr, cmds, cmdCp'access);
                    when others => null;
                end case;
                new_line;
            end if;
            cmds := null;
            exit when to_string(input) = "exit";
        end loop;
        tearDown;
    end mainShell;

    choice : integer;

begin
    loop
        put_line("Which interface would you like to use :");
        new_line;
        put_line("0- EXIT");
        put_line("1- MENU");
        put_line("2- SHELL");
        put("> ");get(choice);
        case choice is
            when 1 =>
                mainMenu;
            when 2 =>
                mainShell;
            when others => null;
        end case;
        exit when choice = 0;
    end loop;
    new_line;
end;

