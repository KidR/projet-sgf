with ada.text_io, ada.integer_text_io;
use ada.text_io, ada.integer_text_io;

with ada.calendar, ada.calendar.formatting, ada.calendar.time_zones;
use ada.calendar, ada.calendar.formatting, ada.calendar.time_zones;


package body datastruct is

    function getOwner(src : in t_access_rights) return integer is

    begin
        return src.owner;
    end getOwner;

    function getGroup(src : in t_access_rights) return integer is

    begin
        return src.group;
    end getGroup;

    function getOther(src : in t_access_rights) return integer is

    begin
        return src.other;
    end getOther;

    function getAccessRights(src : in t_access_rights) return string is

    begin
        return Integer'Image(getOwner(src))(2)&Integer'Image(getGroup(src))(2)&Integer'Image(getOther(src))(2);
    end getAccessRights;

    procedure setAccessRights(src : in out t_access_rights; value : in String) is
        
        function condition(c : in Character) return boolean is
        
        begin
            return Character'Pos(c) < Character'Pos('0') or Character'Pos(c) > Character'Pos('7');
        end condition;

    begin
        if value'Length /= 3 then
            raise RIGHTS_ERROR;
        end if;
        if condition(value(value'First)) or condition(value(value'First+1)) or condition(value(value'First+2)) then
            raise RIGHTS_ERROR;
        end if;
        src.owner := Character'Pos(value(value'First))- Character'Pos('0');
        src.group := Character'Pos(value(value'First + 1)) - Character'Pos('0');
        src.other := Character'Pos(value(value'First + 2))- Character'Pos('0');
    end setAccessRights;

    function image(src : t_access_rights) return string is
        type bitab is array(1..3,1..3) of integer;
        bitmap : bitab;
        rights_str : string := "rwx";
        owner, group, other : integer;
        col, row : integer;
        str : unbounded_string;
    begin
        owner := src.owner;
        group := src.group;
        other := src.other;
        -- R1.1 Convertir la valeur octale en binaire
        for i in 1..3 loop
            bitmap(1,4-i) := owner mod 2;
            bitmap(2,4-i) := group mod 2;
            bitmap(3,4-i) := other mod 2;
            owner := owner / 2;
            group := group / 2;
            other := other / 2;
        end loop;
        -- R1.2 Correspondre la valeur binaire au droit correspondant
        for i in 0..8 loop
            row := Integer(i/3)+1;
            col := i mod 3 + 1;
            if bitmap(row,col ) = 1 then
                -- R1.3 Afficher le droit
                str := str & rights_str(col);
            else
                str := str & to_unbounded_string("-");
            end if;
        end loop;
        return to_string(str);
    end image;

    procedure printAccessRights(src : in t_access_rights) is

    begin
        put(datastruct.Image(src));
    end printAccessRights;

    function timeString(src : in t_date) return string is
        now : time := clock;
    begin
        -- 1..16 prend le format "AAAA-MM-DD HH:MM"
        return Image(Date => ada.calendar.formatting.Time_Of(src.year,
        src.month,
        src.day,
        src.second))(1..16);
    end timeString;

    procedure setNowDate(src : in out t_date) is
        now : time := clock;
    begin
        ada.calendar.split(now,src.year,src.month,src.day,src.second);
    end setNowDate;


end;

