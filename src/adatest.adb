with ada.text_io, ada.integer_text_io;
use ada.text_io, ada.integer_text_io;

with adatest;

package body adatest is

    procedure testname(name : in string) is

    begin
        put_line("### TEST "&name&" ###");
    end testname;

    procedure testresult(res : in integer) is

    begin
        if res = 0 then
            put("K0");
        else
            put("OK");
        end if;
        new_line;
    end testresult;

    procedure testcase(name : in string) is

    begin
        put("- "&name&" : ");
    end testcase;
end;

