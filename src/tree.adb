with ada.text_io, ada.integer_text_io;
use ada.text_io, ada.integer_text_io;

with tree;

package body tree is

    procedure initTree(root : out ptr_node) is

    begin
        root := new t_node;
    end initTree;

    function getRoot(src : in ptr_node) return ptr_node is
        tmp : ptr_node;

        -- R1 TANT QUE le parent du noeud courant n'est pas la racine de l'arbre FAIRE
            -- R2 TANT QUE noeudCourant.parent n'est pas null FAIRE
        -- R1 Le parent du noeud courant devient le noeud courant
            -- R2 noeudCourant <- noeudCourant.parent
        -- R1 Renvoie le noeud courant (qui est la racine)
            -- R2 Return noeudCourant
    begin
        if src = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        tmp := src;
        while tmp.all.parent /= null loop
            tmp := tmp.all.parent;
        end loop;
        return tmp;
    end getRoot;

    procedure setRoot(root: in out ptr_node) is

    begin
        root.all.parent := null;
    end setRoot; 

    function isRoot(src : in ptr_node) return boolean is

    begin
        if src = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        return src.all.parent = null;
    end isRoot;

    function hasChild(src : in ptr_node) return boolean is

    begin
        if src = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        return not list_node.isEmpty(src.all.child);
    end hasChild;

    -- check src's child have node => strong check
    -- instead of check node's parent is src => doesn't cover all cases
    function hasChild(src : in ptr_node; node : in ptr_node) return boolean is
        tmp : ptr_cell;

    -- R1. SI le noeud possede des enfants ALORS
    -- R1. Parcourir tous les enfants tant que je ne suis pas a la fin et que l'enfant soit different de node 
        -- R2. TANT QUE le suivant n'est pas null et que noeud courant /= enfant FAIRE
        -- R2. noeud courant <- noeud suivant;
    -- R1. Retourne vrai si je trouve l'enfant faux sinon
    
    begin
        if src = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        tmp := src.all.child;
        if tmp /= null then
            while tmp.all.next /= null and then tmp.all.current /= node loop
                tmp := tmp.all.next;
            end loop;
            return tmp.all.current = node;
        end if;
        return false;
    end hasChild;

    function hasChild(src : in ptr_node; name : in string) return boolean is
        tmp : ptr_cell;

        -- R1. SI le noeud possede des enfants ALORS
        -- R1. Parcourir tous les enfants tant que je ne suis pas a la fin et que l'enfant ne correspond pas au nom de l'enfant donne 
            -- R2. TANT QUE le suivant n'est pas null et que le nom du noeud courant /= nom de l'enfant FAIRE
            -- R2. noeud courant <- noeud suivant;
        -- R1. Retourne vrai si je trouve l'enfant ayant le bon nom faux sinon
    begin
        if src = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        tmp := src.all.child;
        if tmp /= null then
            while tmp.all.next /= null and then getName(tmp.all.current.all.value) /= name loop
                tmp := tmp.all.next;
            end loop;
            return getName(tmp.all.current.all.value) = name;
        end if;
        return false;
    end hasChild;

    function hasParent(src : in ptr_node; parent : in ptr_node) return boolean is

    begin
        if isRoot(src) then
            raise NODE_ERROR with "root node cannot have parent.";
        end if;
        return src.all.parent = parent;
    end hasParent;

    procedure addChild(src : in out ptr_node; child : in ptr_node) is

        tmp : ptr_cell := new t_cell;

    begin
        if src = null or child = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        if hasChild(src,child) then
            raise NODE_ERROR with "your node already has this child.";
        end if;
        child.all.parent := src;
        list_node.add(src.all.child, child);
    end addChild;

    procedure removeChild(src : in out ptr_node; child : in ptr_node) is
        
        tmp : ptr_cell := new t_cell;

    begin
        if src = null or child = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        list_node.remove(src.all.child, child);
    end removeChild;

    procedure printTree(src : in ptr_node; level : in integer := 0) is

        -- R1 Afficher le nom du noeud courant
        -- R1 TANT QUE j'ai des enfants FAIRE
            -- R2 TANT QUE noeudCourant.enfant n'est pas null FAIRE
        -- R1 PrintTree(noeud enfant du noeud courant)
            -- R2 PrintTree(noeudCourant.enfant)
        -- R1 noeud courant <- noeud suivant
        -- FIN TANT QUE

        procedure pretty(level : in integer) is
            tab : string := "    ";
        begin
            for i in 1..level loop
                put(tab);
            end loop;
            put("* ");
        end pretty;

        tmp : ptr_cell;

    begin
        if src /= null then
            pretty(level);
            put_line(getName(src.all.value));
            tmp := src.all.child;
            while tmp /= null loop
                printTree(tmp.all.current,level+1);
                tmp := tmp.all.next;
            end loop;
        else
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
    end printTree;

    function getPath(src : in ptr_node; node : in ptr_node) return string is
        str : unbounded_string;
        tmp : ptr_node;

    begin
        -- Depart : node ; Arrive : src
        if node /= null then
            if node /= src then
                tmp := node.all.parent;
                str := str & to_unbounded_string(getPath(src,tmp));
            end if;
            str := str & to_unbounded_string(getName(node.all.value)) & "/";
            return to_string(str);
        else
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
    end getPath;

    function getChild(src : in ptr_node; name : in string) return ptr_node is
        tmp : ptr_cell;
    begin
        if src = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        tmp := src.all.child;
        while not list_node.isEmpty(tmp) and then getName(tmp.all.current.all.value) /= name loop
            tmp := tmp.all.next;
        end loop;
        if tmp = null then
            raise NODE_ERROR with STR_NODE_NULL;
        end if;
        return tmp.all.current;
    end getChild;

end;

