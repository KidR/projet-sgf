with ada.text_io, ada.integer_text_io;
use ada.text_io, ada.integer_text_io;

generic
    type T is private; -- element T generique

package list is

    CELL_ERROR : exception; -- exception liee une cellule

    type t_cell; -- cellule d'une liste chainee

    type ptr_cell is access t_cell; -- pointeur de cellule

    -- caracterisation de la cellule
    type t_cell is record
        current : T; -- pointeur sur la cellule courante
        next : ptr_cell; -- pointeur sur la cellule suivante
    end record;

    -- Verifie si une cellule est vide ou non
    -- parametres : src une cellule de liste
    -- retour : boolean vrai si vide faux sinon
    -- post-condition : src = null renvoie vrai
    function isEmpty(src : in ptr_cell) return boolean;

    -- Ajoute une nouvelle cellule d'element e a la liste chainee
    -- parametres : src liste chainee, e l'element a ajouter
    -- pre-condition : src /= null
    -- pos-condition : la  derniere cellule src contient l'element e
    procedure add(src : in out ptr_cell; e: in T);

    -- Supprime l'element e de la liste chainee
    -- parametres : src liste chainee, e l'element a supprimer
    -- exception : CELL_ERROR src est vide et 
    -- pre-condition : src possede l'element e 
    -- post-condition : src ne possede plus l'element e
    procedure remove(src : in out ptr_cell; e: in T);

    -- Supprime l'element en tete de liste
    -- parametres : src liste chainee
    -- pre-condition : src ne soit pas vide
    -- post-condition : deuxieme element de src devienne le premier element apres suppression
    procedure removeFirst(src : in out ptr_cell);

    -- Supprime l'element en queue de liste
    -- parametres : src liste chainee
    -- exception : CELL_ERROR src est vide
    -- post-condition : l'avant dernier element de src devienne le dernier element apres suppression
    procedure removeLast(src : in out ptr_cell);

    -- Supprime et renvoie l'element de la liste chainee se trouvant a la position donnee
    -- parametres : src liste chainee, pos position de l'element
    -- retour : T generique
    -- exception : CELL_ERROR src est vide
    --             CONSTRAINT_ERROR position nulle ou negative
    -- post-condition : element e supprime
    function pop(src : in out ptr_cell; pos : in integer := 1) return T;

    -- Retourne la cellule de la liste chainee d'element e
    -- parametres : src liste chainee, e element
    -- retour : ptr_cell cellule
    -- exception : CELL_ERROR src est vide
    function get(src : in ptr_cell; e : in T) return ptr_cell;

    -- Retourne la cellule de la liste chainee de position pos
    -- parametres : src liste chainee, pos position
    -- retour : ptr_cell cellule
    -- exception : CELL_ERROR src est vide
    function get(src : in ptr_cell; pos : in integer) return ptr_cell;

    -- Retourne la derniere cellule de la liste chainee 
    -- parametres : src la liste chainee
    -- retour : ptr_cell cellule
    -- exception : CELL_ERROR src est vide
    function getLast(src : in ptr_cell) return ptr_cell;

    -- Retourne l'element d'une cellule de liste chainee
    -- parametres : src liste chainee, pos position
    -- retour : T generique
    -- exception : CELL_ERROR src est vide
    function getValue(src : in ptr_cell; pos : in integer) return T;

end;
