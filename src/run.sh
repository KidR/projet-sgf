#/bin/sh

if [ $1 = "-t" ]
then
    clear && echo "\n" && date && gnat make -q $PWD/test_*
    for f in `ls test_*.adb | cut -d . -f 1`
    do
        ./$f
    done
    gnat clean * -q
else
    clear && echo "\n" && date && gnat make -q $PWD/$1 && ./`echo $1 | cut -d . -f 1` && gnat clean * -q
fi