# Manuel utilisateur : système de gestion de fichiers

**Méthodologie de la programmation**
**Projet 2019-2020**

[TOC]

## Comment ça marche ?

Tout d'abord, il est nécessaire de se déplacer dans le répertoire `src` via la commande `cd src`.
Dans ce répertoire se trouvent tous les modules qui composent et permettent le bon fonctionnement du programme.

### Lancement du programme

Pour exécuter le programme principal, il suffit de lancer le script shell mis à disposition de la manière suivante :

```sh
./run.sh main.adb
```

Une fois le script lancé, le programme vous propose de choisir entre deux interfaces utilisateurs 

1. Menu : *user-friendly* avec des indications pour chacune des commandes implémentées
2. Shell : ligne de commande semblable aux terminals Linux

Une fois l'interface utilisateur sélectionné en marche, l'utilisation du système de gestion de fichier est trivial. Si vous n'êtes pas familié avec les commandes UNIX, je vous invite à utiliser le **Menu**. À l'inverse, l'interface **Shell** tend à ressembler à un terminal Linux, notamment par les commandes qu'il implémente (consultable [ici](#Liste-des-commandes)).

### Lancement des tests

Le répertoire `src` contient l'ensemble des tests unitaires de chaque module important. Pour lancer un test, il faut lancer la commande :

```sh
./run.sh test_<nomDuFichierDeTest>.adb
```

Par exemple, pour le test `test_tree.adb` il faut lancer :

```sh
./run.sh test_tree.adb
```

ou bien 

```sh
./run.sh test_tree
```

**Remarque**: il est possible de lancer tous les tests présents dans le dossier `src` de cette manière :

```sh
./run.sh -t
```

## Liste des commandes

Voici les commandes qu'implémente le système de gestion de fichiers :

- `ls` : affiche  l'ensemble  des  fichiers  contenus  dans  chaque répertoire  indiqué. Par défaut, le contenu du répertoire en cours `.` est affiché.
  - `ls nomFichier1 /chemin/du/fichier2 ../dossierParent`
- `ll`: reprend le principe de la commande **ls** qui en plus du nom, affiche le type du fichier, les permissions d'accès, le nom du propriétaire et du groupe, la taille en octets et l'horodatage.
  - `ll nomFichier1 /chemin/du/fichier2 ../dossierParent`
- `cd `: change de répertoire courant.
  - `cd /repertoire/destination`
  - `cd ..`
  - `cd ./repertoire/destination`
- `tree`: affiche l'arborescence du répertoire courant, si aucun chemin n'est indiqué.
  - `tree nomDossier`
  - `tree nomDossier ../secondDossier /dernierDossier`
- `pwd` : affiche le chemin absolu du répertoire courant.
- `touch` : crée un fichier dans le répertoire courant, si aucun chemin n'est indiqué.
  - `touch fic1 fic2`
  - `touch /home/fic1 ../fic2`
- `mkdir` : crée un dossier dans le répertoire courant, si aucun chemin n'est indiqué.
  - `mkdir rep1 rep2`
  - `mkdir /home/rep1 ../rep2`
- `tar` : crée une archive du répertoire courant, si aucun chemin n'est indiqué, en un fichier unique `.tar` dans le répertoire cible.
  - `tar .`
  - `tar /home`
  - `tar ./rep1 ../parentRep`
- `rm` : supprime un fichier du répertoire courant, si aucun chemin n'est indiqué. 
  - `rm fic1`
  - `rm ../ficParent ./fic2 /home/fic3`
- `rr` : supprime récursivement un répertoire ou un fichier.
  - `rr /home/rep1 ../repParent`
- `mv` : déplace un répertoire ou un fichier d'un chemin source vers un chemin destination.
  - `mv fic1 ../repParent`
  - `mv /home rep1`
- `cp` : copie un répertoire ou un fichier d'un chemin source vers un chemin destination.
  - `cp fic1 ../repParent`
  - `cp /home rep1`
- `exit` : quitte le système de gestion de fichiers.

**Remarque** : les commandes sont sensibles à la casse.